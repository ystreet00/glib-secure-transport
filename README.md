# glib-secure-transport
GLib GIO TLS backend using the macOS/iOS Secure Transport API

[glib-networking](https://git.gnome.org/browse/glib-networking) already provides a GIO module that implements a [GIO TLS backend](https://developer.gnome.org/gio/stable/GTlsBackend.html), but it uses [GnuTLS](https://en.wikipedia.org/wiki/GnuTLS).

This project builds a GIO module that uses the [TLS implementation provided by macOS and iOS](https://developer.apple.com/documentation/security/secure_transport?changes=_4&language=objc) as part of the SecureTransport API.

Advantages include reduced external dependencies, easier security updates, and better integration with the macOS/iOS certificate store.

See also [glib-openssl](https://git.gnome.org/browse/glib-openssl/) which does the same but uses OpenSSL.

# Issues

PKCS interface compatibility, SecureTransport seems to only support PKCS#1 keys
("BEGIN RSA PRIVATE KEY") when importing into a SekKeyRef directly.  Which means
that PKCS#8 ("BEGIN PRIVATE KEY") is not supported as a private key format.

To create an SecIdentityRef we need a PKCS#12 blob with both the certificate
and the private key which glib does not provide any conversion functions
to/from other PKCS format.  We are currently using private API
SecIdentityCreate() for now and will be a reason for App rejection from the
Apple App store.

iOS currently does not provide functions to get the 'Not Before' or 'Not After'
values from a SecCertificateRef so if verification returns a date related error
we don't have the information to return the specific case
(G_TLS_CERTIFICATE_EXPIRED or G_TLS_CERTIFICATE_NOT_ACTIVATED) so we return both.

