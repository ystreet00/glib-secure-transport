/*
 * gtlsconnection-securetransport.h
 *
 * Copyright (C) 2018 Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __G_TLS_CONNECTION_SECURE_TRANSPORT_H__
#define __G_TLS_CONNECTION_SECURE_TRANSPORT_H__

#include <gio/gio.h>

#include <Security/Security.h>
#include <Security/SecureTransport.h>

#include "gtlsconnection-base.h"

G_BEGIN_DECLS

#define G_TYPE_TLS_CONNECTION_SECURE_TRANSPORT            (g_tls_connection_secure_transport_get_type ())
#define G_TLS_CONNECTION_SECURE_TRANSPORT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), G_TYPE_TLS_CONNECTION_SECURE_TRANSPORT, GTlsConnectionSecureTransport))
#define G_TLS_CONNECTION_SECURE_TRANSPORT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), G_TYPE_TLS_CONNECTION_SECURE_TRANSPORT, GTlsConnectionSecureTransportClass))
#define G_IS_TLS_CONNECTION_SECURE_TRANSPORT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), G_TYPE_TLS_CONNECTION_SECURE_TRANSPORT))
#define G_IS_TLS_CONNECTION_SECURE_TRANSPORT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), G_TYPE_TLS_CONNECTION_SECURE_TRANSPORT))
#define G_TLS_CONNECTION_SECURE_TRANSPORT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), G_TYPE_TLS_CONNECTION_SECURE_TRANSPORT, GTlsConnectionSecureTransportClass))

typedef struct _GTlsConnectionSecureTransport      GTlsConnectionSecureTransport;
typedef struct _GTlsConnectionSecureTransportClass GTlsConnectionSecureTransportClass;

struct _GTlsConnectionSecureTransport {
  GTlsConnectionBase parent;

  SSLContextRef context;

  GTlsAuthenticationMode authentication_mode;

  gboolean shutting_down;
};

struct _GTlsConnectionSecureTransportClass {
  GTlsConnectionBaseClass parent_class;

  void (*retrieve_cas) (GTlsConnectionSecureTransport * self);
};

GType g_tls_connection_secure_transport_get_type (void);

#ifdef G_DEFINE_AUTOPTR_CLEANUP_FUNC
G_DEFINE_AUTOPTR_CLEANUP_FUNC(GTlsConnectionSecureTransport, g_object_unref)
#endif

G_END_DECLS

#endif /* __G_TLS_CONNECTION_SECURE_TRANSPORT_H__ */

