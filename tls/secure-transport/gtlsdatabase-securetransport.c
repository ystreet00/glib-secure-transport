/*
 * gtlsdatabase-securetransport.c
 *
 * Copyright (C) 2018 Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "config.h"

#include "gtlsdatabase-securetransport.h"
#include "gtlscertificate-securetransport.h"
#include "gtlsutils-securetransport.h"

typedef struct _GTlsDatabaseSecureTransportPrivate
{
  /* protected by mutex */
  GMutex mutex;

  /*
   * These are hash tables of gchar * hashes -> GPtrArray<GBytes>. The values of
   * the ptr array are full DER encoded certificate values. The keys are byte
   * arrays containing either subject DNs, issuer DNs, or full DER encoded certs
   */
  GHashTable *subjects;
  GHashTable *issuers;

  /*
   * This is a table of GBytes -> GBytes. The values and keys are
   * DER encoded certificate values.
   */
  GHashTable *complete;
  /*
   * This is a table of gchar * -> GTlsCertificate.
   */
  GHashTable *certs_by_handle;
} GTlsDatabaseSecureTransportPrivate;

G_DEFINE_TYPE_WITH_CODE (GTlsDatabaseSecureTransport, g_tls_database_secure_transport, G_TYPE_TLS_DATABASE,
                         G_ADD_PRIVATE (GTlsDatabaseSecureTransport))

static GHashTable *
bytes_multi_table_new (void)
{
  return g_hash_table_new_full (g_bytes_hash, g_bytes_equal,
                                (GDestroyNotify)g_bytes_unref,
                                (GDestroyNotify)g_ptr_array_unref);
}

static void
bytes_multi_table_insert (GHashTable *table, GBytes *key, GBytes *value)
{
  GPtrArray *multi;

  g_return_if_fail (key != NULL);

  multi = g_hash_table_lookup (table, key);
  if (multi == NULL) {
    multi = g_ptr_array_new_with_free_func ((GDestroyNotify)g_bytes_unref);
    g_hash_table_insert (table, key, multi);
  }
  g_ptr_array_add (multi, g_bytes_ref (value));
}

static GBytes *
bytes_multi_table_lookup_ref_one (GHashTable *table, const GBytes *key)
{
  GPtrArray *multi;

  multi = g_hash_table_lookup (table, key);
  if (multi == NULL)
    return NULL;

  g_assert (multi->len > 0);
  return g_bytes_ref (multi->pdata[0]);
}

static GList *
bytes_multi_table_lookup_ref_all (GHashTable *table, const GBytes *key)
{
  GPtrArray *multi;
  GList *list = NULL;
  guint i;

  multi = g_hash_table_lookup (table, key);
  if (multi == NULL)
    return NULL;

  for (i = 0; i < multi->len; i++)
    list = g_list_prepend (list, g_bytes_ref (multi->pdata[i]));

  return g_list_reverse (list);
}

static void
g_tls_database_secure_transport_finalize (GObject * obj)
{
  GTlsDatabaseSecureTransport *self = G_TLS_DATABASE_SECURE_TRANSPORT (obj);
  GTlsDatabaseSecureTransportPrivate *priv = g_tls_database_secure_transport_get_instance_private (self);

  if (priv->certs_by_handle) {
    g_hash_table_unref (priv->certs_by_handle);
    priv->certs_by_handle = NULL;
  }

  if (priv->complete) {
    g_hash_table_unref (priv->complete);
    priv->complete = NULL;
  }

  if (priv->subjects) {
    g_hash_table_unref (priv->subjects);
    priv->subjects = NULL;
  }

  if (priv->issuers) {
    g_hash_table_unref (priv->issuers);
    priv->issuers = NULL;
  }

  g_mutex_clear (&priv->mutex);

  G_OBJECT_CLASS (g_tls_database_secure_transport_parent_class)->finalize (obj);
}

static gchar *
create_handle_for_certificate (gchar * filename, GBytes *der)
{
  gchar *bookmark;
  gchar *uri_part;
  gchar *uri;

  /*
   * Here we create a URI that looks like:
   * file:///etc/ssl/certs/ca-certificates.crt#11b2641821252596420e468c275771f5e51022c121a17bd7a89a2f37b6336c8f
   */
  if (filename) {
    uri_part = g_filename_to_uri (filename, NULL, NULL);
    if (!uri_part)
      return NULL;
  } else {
    uri_part = g_strdup ("system://");
  }
  bookmark = g_compute_checksum_for_bytes (G_CHECKSUM_SHA256, der);
  uri = g_strconcat (uri_part, "#", bookmark, NULL);

  g_free (bookmark);
  g_free (uri_part);

  return uri;
}

static GTlsCertificate *
g_tls_database_secure_transport_lookup_certificate_for_handle (GTlsDatabase *database, const gchar *handle,
                                                       GTlsInteraction *interaction, GTlsDatabaseLookupFlags flags,
                                                       GCancellable *cancellable, GError **error)
{
  GTlsDatabaseSecureTransport *self = G_TLS_DATABASE_SECURE_TRANSPORT (database);
  GTlsDatabaseSecureTransportPrivate *priv = g_tls_database_secure_transport_get_instance_private (self);
  GTlsCertificate *cert = NULL;

  g_mutex_lock (&priv->mutex);

  cert = g_hash_table_lookup (priv->certs_by_handle, handle);
  if (cert)
    g_object_ref (cert);

  g_mutex_unlock (&priv->mutex);

  return cert;
}

static GTlsCertificate *
g_tls_database_secure_transport_lookup_certificate_issuer (GTlsDatabase *database, GTlsCertificate *certificate,
                                                   GTlsInteraction *interaction, GTlsDatabaseLookupFlags flags,
                                                   GCancellable *cancellable, GError **error)
{
  GTlsDatabaseSecureTransport *self = G_TLS_DATABASE_SECURE_TRANSPORT (database);
  GTlsDatabaseSecureTransportPrivate *priv = g_tls_database_secure_transport_get_instance_private (self);
  GBytes *issuer_sequence;
  GTlsCertificate *issuer = NULL;
  GBytes *der;

  priv = g_tls_database_secure_transport_get_instance_private (self);

  g_return_val_if_fail (G_IS_TLS_CERTIFICATE_SECURE_TRANSPORT (certificate), NULL);

  if (g_cancellable_set_error_if_cancelled (cancellable, error))
    return NULL;

  if (flags & G_TLS_DATABASE_LOOKUP_KEYPAIR)
    return NULL;

  issuer_sequence = g_tls_certificate_secure_transport_get_issuer_sequence (G_TLS_CERTIFICATE_SECURE_TRANSPORT (certificate));

  g_mutex_lock (&priv->mutex);
  der = bytes_multi_table_lookup_ref_one (priv->subjects, issuer_sequence);
  g_mutex_unlock (&priv->mutex);

  g_bytes_unref (issuer_sequence);

  if (g_cancellable_set_error_if_cancelled (cancellable, error)) {
    issuer = NULL;
  } else if (der != NULL) {
    issuer = g_tls_certificate_secure_transport_new (der, NULL);
  }

  if (der != NULL)
    g_bytes_unref (der);

  return issuer;
}

static GList *
g_tls_database_secure_transport_lookup_certificates_issued_by (GTlsDatabase *database,
    GByteArray *issuer_raw_dn, GTlsInteraction *interaction,
    GTlsDatabaseLookupFlags flags, GCancellable *cancellable, GError **error)
{
  GTlsDatabaseSecureTransport *self = G_TLS_DATABASE_SECURE_TRANSPORT (database);
  GTlsDatabaseSecureTransportPrivate *priv = g_tls_database_secure_transport_get_instance_private (self);
  GQueue issued = G_QUEUE_INIT;
  GBytes *issuer = NULL;
  GList *ders, *l;

  if (flags & G_TLS_DATABASE_LOOKUP_KEYPAIR)
    return NULL;

  issuer = g_bytes_new (issuer_raw_dn->data, issuer_raw_dn->len);

  /* Find the full DER value of the certificate */
  g_mutex_lock (&priv->mutex);
  ders = bytes_multi_table_lookup_ref_all (priv->issuers, issuer);
  g_mutex_unlock (&priv->mutex);

  g_bytes_unref (issuer);
  issuer = NULL;

  for (l = ders; l != NULL; l = g_list_next (l)) {
    if (g_cancellable_set_error_if_cancelled (cancellable, error)) {
      g_queue_free_full (&issued, g_object_unref);
      break;
    }

    g_queue_push_tail (&issued, g_tls_certificate_secure_transport_new (l->data, NULL));
  }

  g_list_free_full (ders, (GDestroyNotify) g_bytes_unref);

  return issued.head;
}

static void
_hash_table_item_cert_to_list_ref (gpointer key, gpointer value, GList ** list)
{
  GTlsCertificateSecureTransport *cert = (GTlsCertificateSecureTransport *) value;
  *list = g_list_prepend (*list, g_object_ref (cert));
}

static GTlsCertificateFlags
g_tls_database_secure_transport_verify_chain (GTlsDatabase *database,
    GTlsCertificate *chain, const gchar *purpose, GSocketConnectable *identity,
    GTlsInteraction *interaction, GTlsDatabaseVerifyFlags flags,
    GCancellable *cancellable, GError **error)
{
  GTlsDatabaseSecureTransport *self = G_TLS_DATABASE_SECURE_TRANSPORT (database);
  GTlsDatabaseSecureTransportPrivate *priv = g_tls_database_secure_transport_get_instance_private (self);
  GTlsCertificateFlags ret = 0;
  GList *anchor_certs = NULL;

  g_mutex_lock (&priv->mutex);
  g_hash_table_foreach (priv->certs_by_handle,
      (GHFunc) _hash_table_item_cert_to_list_ref, (gpointer) &anchor_certs);
  g_mutex_unlock (&priv->mutex);

  ret = g_tls_certificate_secure_transport_verify_internal (G_TLS_CERTIFICATE_SECURE_TRANSPORT (chain), identity, purpose, anchor_certs);

  g_list_free_full (anchor_certs, (GDestroyNotify) g_object_unref);

  return ret;
}

CFArrayRef
g_tls_database_secure_transport_to_cert_array (GTlsDatabaseSecureTransport * self)
{
  GTlsDatabaseSecureTransportPrivate *priv = g_tls_database_secure_transport_get_instance_private (self);
  CFMutableArrayRef certs = NULL;
  GList *anchor_certs = NULL, *l;

  g_mutex_lock (&priv->mutex);
  g_hash_table_foreach (priv->certs_by_handle,
      (GHFunc) _hash_table_item_cert_to_list_ref, (gpointer) &anchor_certs);
  g_mutex_unlock (&priv->mutex);

  certs = CFArrayCreateMutable (NULL, 0, &kCFTypeArrayCallBacks);
  for (l = anchor_certs; l; l = g_list_next (l)) {
    GTlsCertificateSecureTransport *cert = G_TLS_CERTIFICATE_SECURE_TRANSPORT (l->data);
    CFArrayAppendValue (certs, g_tls_certificate_secure_transport_get_cert (cert));
  }

  g_list_free_full (anchor_certs, (GDestroyNotify) g_object_unref);

  return certs;
}

static void
g_tls_database_secure_transport_class_init (GTlsDatabaseSecureTransportClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;
  GTlsDatabaseClass *database_class = (GTlsDatabaseClass *) klass;

  gobject_class->finalize = g_tls_database_secure_transport_finalize;

  database_class->lookup_certificate_for_handle = g_tls_database_secure_transport_lookup_certificate_for_handle;
  database_class->lookup_certificate_issuer = g_tls_database_secure_transport_lookup_certificate_issuer;
  database_class->lookup_certificates_issued_by = g_tls_database_secure_transport_lookup_certificates_issued_by;
  database_class->verify_chain = g_tls_database_secure_transport_verify_chain;
}

static void
g_tls_database_secure_transport_init (GTlsDatabaseSecureTransport *self)
{
  GTlsDatabaseSecureTransportPrivate *priv;

  priv = g_tls_database_secure_transport_get_instance_private (self);

  priv->certs_by_handle = g_hash_table_new_full (g_str_hash, g_str_equal,
                                    (GDestroyNotify)g_free,
                                    (GDestroyNotify)g_bytes_unref);

  priv->complete = g_hash_table_new_full (g_bytes_hash, g_bytes_equal,
                                    (GDestroyNotify)g_bytes_unref,
                                    (GDestroyNotify)g_bytes_unref);

  priv->subjects = bytes_multi_table_new ();
  priv->issuers = bytes_multi_table_new ();

  g_mutex_init (&priv->mutex);
}

static void
g_tls_database_secure_transport_load_cert_list (GTlsDatabaseSecureTransport * self, GList * list, gchar * filename)
{
  GTlsDatabaseSecureTransportPrivate *priv = g_tls_database_secure_transport_get_instance_private (G_TLS_DATABASE_SECURE_TRANSPORT (self));
  GList *l;

  for (l = list; l; l = l->next) {
    GTlsCertificateSecureTransport *cert = G_TLS_CERTIFICATE_SECURE_TRANSPORT (l->data);
    GBytes *der = g_tls_certificate_secure_transport_get_cert_bytes (cert);
    GBytes *issuer = g_tls_certificate_secure_transport_get_issuer_sequence (cert);
    GBytes *subject = g_tls_certificate_secure_transport_get_subject_sequence (cert);
    gchar *handle;

    g_return_if_fail (der != NULL);
    g_return_if_fail (issuer != NULL);
    g_return_if_fail (subject != NULL);

    g_hash_table_insert (priv->complete, g_bytes_ref (der),
                         g_bytes_ref (der));

    bytes_multi_table_insert (priv->subjects, subject, der);
    bytes_multi_table_insert (priv->issuers, issuer, der);

    handle = create_handle_for_certificate (filename, der);
    g_debug ("%p loaded certificate with handle %s", self, handle);
    g_hash_table_insert (priv->certs_by_handle, handle, g_object_ref (cert));
  }
}

enum {
  PROP_FILE_DB_ANCHORS = 1,
  PROP_FILE_DB_N_PROPERTIES
};

struct _GTlsFileDatabaseSecureTransport {
  GTlsDatabaseSecureTransport parent;
};

typedef struct _GTlsFileDatabaseSecureTransportPrivate {
  gchar *anchors;
} GTlsFileDatabaseSecureTransportPrivate;

static void g_tls_file_database_secure_transport_initable_interface_init (GInitableIface *iface);
static void g_tls_file_database_secure_transport_file_database_interface_init (GTlsFileDatabaseInterface *iface);

G_DEFINE_TYPE_WITH_CODE (GTlsFileDatabaseSecureTransport, g_tls_file_database_secure_transport, G_TYPE_TLS_DATABASE_SECURE_TRANSPORT,
                         G_ADD_PRIVATE (GTlsFileDatabaseSecureTransport)
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE, g_tls_file_database_secure_transport_initable_interface_init)
                         G_IMPLEMENT_INTERFACE (G_TYPE_TLS_FILE_DATABASE, g_tls_file_database_secure_transport_file_database_interface_init))

static void
g_tls_file_database_secure_transport_load (GTlsFileDatabaseSecureTransport *self)
{
  GTlsFileDatabaseSecureTransportPrivate *priv = g_tls_file_database_secure_transport_get_instance_private (self);
  GError *my_error = NULL;
  GList *list;

  if (!priv->anchors)
    return;

  list = g_tls_certificate_list_new_from_file (priv->anchors, &my_error);
  if (my_error) {
    g_warning ("%s", my_error->message);
    return;
  }

  g_tls_database_secure_transport_load_cert_list (G_TLS_DATABASE_SECURE_TRANSPORT (self), list, priv->anchors);
}

static gchar *
g_tls_file_database_secure_transport_create_certificate_handle (GTlsDatabase *database, GTlsCertificate *certificate)
{
  GTlsFileDatabaseSecureTransport *self = G_TLS_FILE_DATABASE_SECURE_TRANSPORT (database);
  GTlsFileDatabaseSecureTransportPrivate *priv;
  GTlsDatabaseSecureTransportPrivate *db_priv;
  GBytes *der;
  gboolean contains;
  gchar *handle = NULL;

  priv = g_tls_file_database_secure_transport_get_instance_private (self);
  db_priv = g_tls_database_secure_transport_get_instance_private (G_TLS_DATABASE_SECURE_TRANSPORT (self));

  der = g_tls_certificate_secure_transport_get_cert_bytes (G_TLS_CERTIFICATE_SECURE_TRANSPORT (certificate));
  g_return_val_if_fail (der != NULL, FALSE);

  g_mutex_lock (&db_priv->mutex);
  /* At the same time look up whether this certificate is in list */
  contains = g_hash_table_lookup (db_priv->complete, der) ? TRUE : FALSE;
  g_mutex_unlock (&db_priv->mutex);

  /* Certificate is in the database */
  if (contains)
    handle = create_handle_for_certificate (priv->anchors, der);

  g_bytes_unref (der);
  return handle;
}

static void
g_tls_file_database_secure_transport_set_property (GObject * obj, guint property_id, const GValue *value, GParamSpec *pspec)
{
  GTlsFileDatabaseSecureTransport *self = G_TLS_FILE_DATABASE_SECURE_TRANSPORT (obj);
  GTlsFileDatabaseSecureTransportPrivate *priv = g_tls_file_database_secure_transport_get_instance_private (self);
  GTlsDatabaseSecureTransportPrivate *db_priv = g_tls_database_secure_transport_get_instance_private (G_TLS_DATABASE_SECURE_TRANSPORT (self));

  switch (property_id) {
    case PROP_FILE_DB_ANCHORS:
      /* Remove all old certificates, if any */
      g_hash_table_remove_all (db_priv->complete);
      g_hash_table_remove_all (db_priv->subjects);
      g_hash_table_remove_all (db_priv->issuers);
      g_hash_table_remove_all (db_priv->certs_by_handle);
      g_free (priv->anchors);
      priv->anchors = g_value_dup_string (value);
      g_tls_file_database_secure_transport_load (self);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, property_id, pspec);
      break;
  }
}

static void
g_tls_file_database_secure_transport_get_property (GObject * obj, guint property_id, GValue *value, GParamSpec *pspec)
{
  GTlsFileDatabaseSecureTransport *self = G_TLS_FILE_DATABASE_SECURE_TRANSPORT (obj);
  GTlsFileDatabaseSecureTransportPrivate *priv = g_tls_file_database_secure_transport_get_instance_private (self);

  switch (property_id) {
    case PROP_FILE_DB_ANCHORS:
      g_value_set_string (value, priv->anchors);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, property_id, pspec);
      break;
  }
}

static void
g_tls_file_database_secure_transport_finalize (GObject * obj)
{
  GTlsFileDatabaseSecureTransport *self = G_TLS_FILE_DATABASE_SECURE_TRANSPORT (obj);
  GTlsFileDatabaseSecureTransportPrivate *priv = g_tls_file_database_secure_transport_get_instance_private (self);

  g_free (priv->anchors);
  priv->anchors = NULL;

  G_OBJECT_CLASS (g_tls_file_database_secure_transport_parent_class)->finalize (obj);
}

static void
g_tls_file_database_secure_transport_class_init (GTlsFileDatabaseSecureTransportClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;
  GTlsDatabaseClass *database_class = (GTlsDatabaseClass *) klass;

  gobject_class->set_property = g_tls_file_database_secure_transport_set_property;
  gobject_class->get_property = g_tls_file_database_secure_transport_get_property;
  gobject_class->finalize = g_tls_file_database_secure_transport_finalize;

  g_object_class_override_property (gobject_class, PROP_FILE_DB_ANCHORS, "anchors");

  database_class->create_certificate_handle = g_tls_file_database_secure_transport_create_certificate_handle;
}

static gboolean
g_tls_file_database_secure_transport_initable_init (GInitable *initable, GCancellable *cancellable, GError **error)
{
  return TRUE;
}

static void
g_tls_file_database_secure_transport_initable_interface_init (GInitableIface *iface)
{
  iface->init = g_tls_file_database_secure_transport_initable_init;
}

static void
g_tls_file_database_secure_transport_file_database_interface_init (GTlsFileDatabaseInterface *iface)
{
}

static void
g_tls_file_database_secure_transport_init (GTlsFileDatabaseSecureTransport *self)
{
}

GTlsDatabase *
g_tls_file_database_secure_transport_new (const gchar * filename)
{
  return g_object_new (G_TYPE_TLS_FILE_DATABASE_SECURE_TRANSPORT, "anchors", filename, NULL);
}

struct _GTlsSystemDatabaseSecureTransport {
  GTlsDatabaseSecureTransport parent;
};

typedef struct _GTlsSystemDatabaseSecureTransportPrivate {
  gint dummy;
} GTlsSystemDatabaseSecureTransportPrivate;

static void g_tls_system_database_secure_transport_initable_interface_init (GInitableIface *iface);

G_DEFINE_TYPE_WITH_CODE (GTlsSystemDatabaseSecureTransport, g_tls_system_database_secure_transport, G_TYPE_TLS_DATABASE_SECURE_TRANSPORT,
                         G_ADD_PRIVATE (GTlsSystemDatabaseSecureTransport)
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE, g_tls_system_database_secure_transport_initable_interface_init))

static void
g_tls_system_database_secure_transport_load (GTlsSystemDatabaseSecureTransport *self)
{
  GList *list = NULL;

#if TARGET_OS_MAC && !TARGET_OS_IPHONE
  {
    OSStatus status;
    CFArrayRef certs = NULL;
    int i;

    status = SecTrustCopyAnchorCertificates (&certs);
    if (!warn_osstatus(status))
      return;

    if (certs) {
      for (i = 0; i < CFArrayGetCount (certs); i++) {
        SecCertificateRef cert_ref = (SecCertificateRef) CFArrayGetValueAtIndex(certs, i);
        GTlsCertificate *cert;
        CFDataRef data;
        GBytes *bytes;

        data = SecCertificateCopyData (cert_ref);
        bytes = g_bytes_new_with_free_func (CFDataGetBytePtr (data), CFDataGetLength (data),
            (GDestroyNotify) CFRelease, data);

        cert = g_tls_certificate_secure_transport_new (bytes, NULL);
        list = g_list_prepend (list, cert);

        CFRelease (data);
        g_bytes_unref (bytes);
      }

      CFRelease (certs);
    }

    list = g_list_reverse (list);
  }
#endif
#if TARGET_OS_IPHONE
  g_warning("Don't know how to get the default trusted CA list on iOS");
  list = NULL;
#endif
  if (!list)
    g_warning ("No system certs available");

  g_tls_database_secure_transport_load_cert_list (G_TLS_DATABASE_SECURE_TRANSPORT (self), list, NULL);
}

static gchar *
g_tls_system_database_secure_transport_create_certificate_handle (GTlsDatabase *database, GTlsCertificate *certificate)
{
  GTlsSystemDatabaseSecureTransport *self = G_TLS_SYSTEM_DATABASE_SECURE_TRANSPORT (database);
  GTlsDatabaseSecureTransportPrivate *db_priv;
  GBytes *der;
  gboolean contains;
  gchar *handle = NULL;

  db_priv = g_tls_database_secure_transport_get_instance_private (G_TLS_DATABASE_SECURE_TRANSPORT (self));

  der = g_tls_certificate_secure_transport_get_cert_bytes (G_TLS_CERTIFICATE_SECURE_TRANSPORT (certificate));
  g_return_val_if_fail (der != NULL, FALSE);

  g_mutex_lock (&db_priv->mutex);
  /* At the same time look up whether this certificate is in list */
  contains = g_hash_table_lookup (db_priv->complete, der) ? TRUE : FALSE;
  g_mutex_unlock (&db_priv->mutex);

  /* Certificate is in the database */
  if (contains)
    handle = create_handle_for_certificate (NULL, der);

  g_bytes_unref (der);
  return handle;
}

static void
g_tls_system_database_secure_transport_class_init (GTlsSystemDatabaseSecureTransportClass *klass)
{
  GTlsDatabaseClass *database_class = (GTlsDatabaseClass *) klass;

  database_class->create_certificate_handle = g_tls_system_database_secure_transport_create_certificate_handle;
}

static gboolean
g_tls_system_database_secure_transport_initable_init (GInitable *initable, GCancellable *cancellable, GError **error)
{
  return TRUE;
}

static void
g_tls_system_database_secure_transport_initable_interface_init (GInitableIface *iface)
{
  iface->init = g_tls_system_database_secure_transport_initable_init;
}

static void
g_tls_system_database_secure_transport_init (GTlsSystemDatabaseSecureTransport *self)
{
  g_tls_system_database_secure_transport_load (self);
}

GTlsDatabase *
g_tls_system_database_secure_transport_new ()
{
  return g_object_new (G_TYPE_TLS_SYSTEM_DATABASE_SECURE_TRANSPORT, NULL);
}

