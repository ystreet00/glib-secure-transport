/*
 * gtlsutils-securetransport.h
 *
 * Copyright (C) 2018 Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __G_TLS_UTILS_SECURE_TRANSPORT_H__
#define __G_TLS_UTILS_SECURE_TRANSPORT_H__

#include <gio/gio.h>

#include <Security/Security.h>
#include <Security/SecTrust.h>

G_BEGIN_DECLS

GTlsCertificateFlags secure_transport_trust_evaluate                             (SecTrustRef trust);
gchar *              g_tls_secure_transport_socket_connectable_to_string         (GSocketConnectable *identity);

gboolean             pem_to_der                                                  (const guint8 * data, gsize length, CFDataRef * out);
guint8 *             der_to_pem                                                  (CFDataRef der, gsize * length);

char *               osstatus_to_string                                          (OSStatus status);
gboolean             check_osstatus                                              (OSStatus status);
gboolean             warn_osstatus                                               (OSStatus status);

char *               cf_string_copy_to_utf8                                      (CFStringRef str);

G_END_DECLS

#endif /* __G_TLS_UTILS_SECURE_TRANSPORT_H__ */

