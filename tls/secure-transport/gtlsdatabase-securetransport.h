/*
 * gtlsdatabase-securetransport.h
 *
 * Copyright (C) 2018 Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __G_TLS_DATABASE_SECURE_TRANSPORT_H__
#define __G_TLS_DATABASE_SECURE_TRANSPORT_H__

#include <gio/gio.h>

#include <Security/Security.h>

G_BEGIN_DECLS

#define G_TYPE_TLS_DATABASE_SECURE_TRANSPORT (g_tls_database_secure_transport_get_type ())
G_DECLARE_DERIVABLE_TYPE (GTlsDatabaseSecureTransport, g_tls_database_secure_transport,
                          G, TLS_DATABASE_SECURE_TRANSPORT, GTlsDatabase)

struct _GTlsDatabaseSecureTransportClass {
  GTlsDatabaseClass parent_class;
};

CFArrayRef     g_tls_database_secure_transport_to_cert_array     (GTlsDatabaseSecureTransport * self);

#define G_TYPE_TLS_SYSTEM_DATABASE_SECURE_TRANSPORT (g_tls_system_database_secure_transport_get_type ())
G_DECLARE_FINAL_TYPE (GTlsSystemDatabaseSecureTransport, g_tls_system_database_secure_transport,
                      G, TLS_SYSTEM_DATABASE_SECURE_TRANSPORT, GTlsDatabaseSecureTransport)

GTlsDatabase * g_tls_system_database_secure_transport_new (void);

#define G_TYPE_TLS_FILE_DATABASE_SECURE_TRANSPORT (g_tls_file_database_secure_transport_get_type ())
G_DECLARE_FINAL_TYPE (GTlsFileDatabaseSecureTransport, g_tls_file_database_secure_transport,
                      G, TLS_FILE_DATABASE_SECURE_TRANSPORT, GTlsDatabaseSecureTransport)

GTlsDatabase * g_tls_file_database_secure_transport_new (const gchar * filename);

G_END_DECLS

#endif /* __G_TLS_DATABASE_SECURE_TRANSPORT_H__ */
