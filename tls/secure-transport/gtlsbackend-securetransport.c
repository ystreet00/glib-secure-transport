/*
 * gtlsbackend-securetransport.c
 *
 * Copyright (C) 2018 Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "config.h"

#include "gtlsbackend-securetransport.h"
#include "gtlsdatabase-securetransport.h"
#include "gtlscertificate-securetransport.h"
#include "gtlsclientconnection-securetransport.h"
#include "gtlsserverconnection-securetransport.h"

struct _GTlsBackendSecureTransport
{
  GObject parent;
};

static void g_tls_backend_secure_transport_interface_init (GTlsBackendInterface *iface);

G_DEFINE_DYNAMIC_TYPE_EXTENDED (GTlsBackendSecureTransport, g_tls_backend_secure_transport, G_TYPE_OBJECT, 0,
                                G_IMPLEMENT_INTERFACE_DYNAMIC (G_TYPE_TLS_BACKEND,
                                                               g_tls_backend_secure_transport_interface_init))

static void
g_tls_backend_secure_transport_init (GTlsBackendSecureTransport *backend)
{
}

static void
g_tls_backend_secure_transport_class_init (GTlsBackendSecureTransportClass *klass)
{
}

static void
g_tls_backend_secure_transport_class_finalize (GTlsBackendSecureTransportClass *backend_class)
{
}

static gboolean
g_tls_backend_secure_transport_supports_tls (GTlsBackend *backend)
{
  return TRUE;
}

static gboolean
g_tls_backend_secure_transport_supports_dtls (GTlsBackend *backend)
{
  return FALSE;
}

static GTlsDatabase *
g_tls_backend_secure_transport_get_default_database (GTlsBackend *backend)
{
  G_LOCK_DEFINE_STATIC (default_database);
  static GTlsDatabase *database = NULL;

  G_LOCK (default_database);
  if (!database)
    database = g_tls_system_database_secure_transport_new ();
  G_UNLOCK (default_database);

  return g_object_ref (database);
}

static void
g_tls_backend_secure_transport_interface_init (GTlsBackendInterface *iface)
{
  iface->supports_tls = g_tls_backend_secure_transport_supports_tls;
  iface->supports_dtls = g_tls_backend_secure_transport_supports_dtls;
  iface->get_default_database = g_tls_backend_secure_transport_get_default_database;
  iface->get_file_database_type = g_tls_file_database_secure_transport_get_type;
  iface->get_certificate_type = g_tls_certificate_secure_transport_get_type;
  iface->get_client_connection_type = g_tls_client_connection_secure_transport_get_type;
  iface->get_server_connection_type = g_tls_server_connection_secure_transport_get_type;
}

void
g_tls_backend_secure_transport_register (GIOModule *module)
{
  GTypePlugin *plugin;

  g_tls_backend_secure_transport_register_type (G_TYPE_MODULE (module));
  if (module == NULL) {
    g_io_extension_point_register (G_TLS_BACKEND_EXTENSION_POINT_NAME);
  }
  g_io_extension_point_implement (G_TLS_BACKEND_EXTENSION_POINT_NAME,
                                  g_tls_backend_secure_transport_get_type(),
                                  "SecureTransport",
                                  100);

  plugin = g_type_get_plugin (G_TYPE_TLS_BACKEND_SECURE_TRANSPORT);
  if (plugin)
    g_type_plugin_use (plugin);
}
