/*
 * gtlsutils-securetransport.c
 *
 * Copyright (C) 2018 Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "config.h"

#include "gtlsutils-securetransport.h"

gchar *
g_tls_secure_transport_socket_connectable_to_string (GSocketConnectable *identity)
{
  gchar *server_name;

  if (G_IS_NETWORK_ADDRESS (identity)) {
    server_name = g_strdup (g_network_address_get_hostname (G_NETWORK_ADDRESS (identity)));
  } else if (G_IS_NETWORK_SERVICE (identity)) {
    server_name = g_strdup (g_network_service_get_domain (G_NETWORK_SERVICE (identity)));
  } else if (G_IS_INET_SOCKET_ADDRESS (identity)) {
    GInetAddress * addr = g_inet_socket_address_get_address (G_INET_SOCKET_ADDRESS (identity));
    server_name = g_inet_address_to_string (addr);
  } else {
    g_warn_if_reached ();
    server_name = NULL;
  }

  return server_name;
}

gboolean
pem_to_der (const guint8 * data, gsize length, CFDataRef * out)
{
  CFDataRef ret = NULL;
  guint8 *der = NULL;
  gsize decoded_length;
  gint state = 0;
  guint save = 0;

  der = g_new (guint8, length);

  decoded_length = g_base64_decode_step ((const gchar *) data, length, der, &state, &save);
  if (!decoded_length) {
    g_free (der);
    return FALSE;
  }

  ret = CFDataCreate (NULL, der, (CFIndex) decoded_length);
  if (!ret) {
    g_free (der);
    return FALSE;
  }

  g_free (der);
  *out = ret;
  return TRUE;
}

guint8 *
der_to_pem (CFDataRef der, gsize * length)
{
  guint8 *ret = NULL;

  if (!der)
    return NULL;

  ret = (guint8 *) g_base64_encode (CFDataGetBytePtr(der), CFDataGetLength(der));
  if (!ret)
    return NULL;

  if (length)
    *length = strlen((gchar *) ret);
  return ret;
}

char *
osstatus_to_string (OSStatus status)
{
#if TARGET_OS_IPHONE && !defined(__IPHONE_11_3)
  return g_strdup_printf ("0x%x: unknown (Error to string API unavailable)",
      (unsigned int) status);
#else
  CFStringRef message = SecCopyErrorMessageString (status, NULL);
  return cf_string_copy_to_utf8 (message);
#endif
}

gboolean
check_osstatus (OSStatus status)
{
  return status == errSecSuccess;
}

gboolean
warn_osstatus (OSStatus status)
{
  if (!check_osstatus (status)) {
    char *msg = osstatus_to_string (status);
    g_warning ("%s\n", msg);
    g_free (msg);
    return FALSE;
  }
  return TRUE;
}

char *
cf_string_copy_to_utf8(CFStringRef str)
{
  CFIndex length, max_size;
  char *ret;

  if (str == NULL) {
    return NULL;
  }

  length = CFStringGetLength(str);
  max_size = CFStringGetMaximumSizeForEncoding(length, kCFStringEncodingUTF8) + 1;
  ret = g_new0 (char, max_size);
  if (CFStringGetCString(str, ret, max_size,
                         kCFStringEncodingUTF8)) {
    return ret;
  }
  g_free (ret);
  return NULL;
}

#if TARGET_OS_MAC && !TARGET_OS_IPHONE
static GTlsCertificateFlags
validate_cert_start_stop (SecCertificateRef cert, CFDateRef reference_date)
{
  CFArrayRef key_selection;
  CFDictionaryRef vals;
  CFDictionaryRef dict;
  CFNumberRef absolute_time;
  CFErrorRef error;
  CFStringRef keys[] = {kSecOIDX509V1ValidityNotAfter, kSecOIDX509V1ValidityNotBefore};
  GTlsCertificateFlags ret = 0;
  CFDateRef cert_value;
  double seconds_since_epoch;

  /* The not before and not after OID's return numbers since 1 Jan 2018 0:00 GMT
   * which is conveniently also the epoch for CFAbsoluteTime used as the value
   * to CFDateCreate()  */

  key_selection = CFArrayCreate(NULL, (const void **) keys, 2, &kCFTypeArrayCallBacks);
  vals = SecCertificateCopyValues(cert, key_selection, &error);
  dict = CFDictionaryGetValue(vals, keys[0]);
  absolute_time = CFDictionaryGetValue(dict, kSecPropertyKeyValue);
  CFNumberGetValue (absolute_time, kCFNumberFloat64Type, &seconds_since_epoch);
  cert_value = CFDateCreate (NULL, seconds_since_epoch);

  /* if the reference date is later then the not after date */
  if (CFDateGetTimeIntervalSinceDate (cert_value, reference_date) < 0)
    ret |= G_TLS_CERTIFICATE_EXPIRED;

  CFRelease (cert_value);

  dict = CFDictionaryGetValue(vals, keys[1]);
  absolute_time = CFDictionaryGetValue(dict, kSecPropertyKeyValue);
  CFNumberGetValue (absolute_time, kCFNumberFloat64Type, &seconds_since_epoch);
  cert_value = CFDateCreate (NULL, seconds_since_epoch);

  /* if the reference date is earlier than the not before date */
  if (CFDateGetTimeIntervalSinceDate (cert_value, reference_date) > 0)
    ret |= G_TLS_CERTIFICATE_NOT_ACTIVATED;

  CFRelease (cert_value);

  CFRelease(vals);
  CFRelease(key_selection);

  return ret;
}
#endif

static GTlsCertificateFlags
trust_evaluation_to_flags (SecTrustResultType result, CFDictionaryRef results, CFArrayRef properties, CFDataRef exceptions)
{
  GTlsCertificateFlags ret = 0;

  /* we did something wrong */
  g_return_val_if_fail (result != kSecTrustResultInvalid, G_TLS_CERTIFICATE_GENERIC_ERROR);

  /* pass status */
  if (result == kSecTrustResultUnspecified || result == kSecTrustResultProceed)
    return 0;

  if (result == kSecTrustResultOtherError)
    ret |= G_TLS_CERTIFICATE_GENERIC_ERROR;

  if (results) {
    CFStringRef details_key_name = CFSTR("TrustResultDetails");
    CFArrayRef detail_list = NULL;

    if (CFDictionaryGetValueIfPresent (results, details_key_name, (const void **) &detail_list)) {
      int i;

      for (i = 0; i < CFArrayGetCount (detail_list); i++) {
        CFDictionaryRef details = CFArrayGetValueAtIndex (detail_list, i);
        CFStringRef anchor_trusted_key_name = CFSTR("AnchorTrusted");
        CFBooleanRef anchor_trusted;
        CFStringRef ssl_hostname_key_name = CFSTR("SSLHostname");
        CFBooleanRef ssl_hostname;

        if (CFDictionaryGetValueIfPresent (details, anchor_trusted_key_name, (const void **) &anchor_trusted)) {
          if (!CFBooleanGetValue (anchor_trusted))
            ret |= G_TLS_CERTIFICATE_UNKNOWN_CA;
        }
        if (CFDictionaryGetValueIfPresent (details, ssl_hostname_key_name, (const void **) &ssl_hostname)) {
          if (!CFBooleanGetValue (ssl_hostname))
            ret |= G_TLS_CERTIFICATE_BAD_IDENTITY;
        }
      }
    }
  }

  if (properties) {
    int i;

    for (i = 0; i < CFArrayGetCount (properties); i++) {
      CFStringRef err = CFSTR("error");
      CFDictionaryRef dict;
      CFStringRef err_str = NULL;

      dict = (CFDictionaryRef) CFArrayGetValueAtIndex (properties, i);

      if (CFDictionaryGetValueIfPresent (dict, err, (const void **) &err_str)) {
        gchar *err_c_str = cf_string_copy_to_utf8 (err_str);

        if (g_strcmp0 (err_c_str, "Host name mismatch") == 0)
          ret |= G_TLS_CERTIFICATE_BAD_IDENTITY;
#if TARGET_OS_IPHONE
        /* not really useful as it doesn't say whether we're before or after
         * however this is all we have for iOS */
        if (g_strcmp0 (err_c_str, "CSSMERR_TP_CERT_EXPIRED") == 0)
          ret |= G_TLS_CERTIFICATE_NOT_ACTIVATED | G_TLS_CERTIFICATE_EXPIRED;
#endif
        /* XXX: add more failure cases as we go on */

        g_free (err_c_str);
      }
    }
  }

  return ret;
}

GTlsCertificateFlags
secure_transport_trust_evaluate (SecTrustRef trust)
{
  GTlsCertificateFlags ret = 0;
  CFDictionaryRef results = NULL;
  CFArrayRef properties = NULL;
  CFDataRef exceptions = NULL;
  SecTrustResultType result;
  OSStatus status;

  status = SecTrustEvaluate (trust, &result);
  if (!warn_osstatus(status)) {
    ret = G_TLS_CERTIFICATE_GENERIC_ERROR;
    goto out;
  }

  results = SecTrustCopyResult (trust);
  properties = SecTrustCopyProperties (trust);
  exceptions = SecTrustCopyExceptions (trust);

  ret = trust_evaluation_to_flags (result, results, properties, exceptions);
  {
#if TARGET_OS_MAC && !TARGET_OS_IPHONE
    GTlsCertificateFlags time_flags = 0;
    CFDateRef date;
    int i;

    if (CFDictionaryGetValueIfPresent (results, CFSTR("TrustEvaluationDate"), (const void **) &date)) {
      for (i = 0; i < SecTrustGetCertificateCount (trust); i++) {
        SecCertificateRef cert_ref = (SecCertificateRef) SecTrustGetCertificateAtIndex (trust, i);
        time_flags |= validate_cert_start_stop (cert_ref, date);
      }

      ret |= time_flags;
    }
#endif
  }

out:
  if (properties)
    CFRelease (properties);
  if (results)
    CFRelease (results);
  if (results)
    CFRelease (exceptions);

  return ret;
}
