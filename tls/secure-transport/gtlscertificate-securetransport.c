/*
 * gtlscertificate-securetransport.c
 *
 * Copyright (C) 2018 Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "config.h"

#include "gtlscertificate-securetransport.h"
#include "gtlsutils-securetransport.h"

#define CERT_PREFIX "-----BEGIN CERTIFICATE-----\n"
#define CERT_SUFFIX "\n-----END CERTIFICATE-----"
#define KEY_PREFIX "-----BEGIN RSA PRIVATE KEY-----\n"
#define KEY_SUFFIX "\n-----END RSA PRIVATE KEY-----"
/* XXX: PKCS#8 (BEGIN/END PRIVATE KEY) doesn't seem to work!
 * SecCertificateCreateWithData() returns NULL */

enum {
  PROP_CERTIFICATE = 1,
  PROP_CERTIFICATE_PEM,
  PROP_ISSUER,
  PROP_PRIVATE_KEY,
  PROP_PRIVATE_KEY_PEM,
};

struct _GTlsCertificateSecureTransport {
  GTlsCertificate parent;
};

typedef struct _GTlsCertificateSecureTransportPrivate {
  gchar *key_pem;
  SecKeyRef private_key;
  gchar *cert_pem;
  SecCertificateRef certificate;
  GTlsCertificate *issuer;
} GTlsCertificateSecureTransportPrivate;

static void g_tls_certificate_secure_transport_initable_interface_init (GInitableIface *iface);

G_DEFINE_TYPE_WITH_CODE (GTlsCertificateSecureTransport, g_tls_certificate_secure_transport, G_TYPE_TLS_CERTIFICATE,
                         G_ADD_PRIVATE (GTlsCertificateSecureTransport)
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE, g_tls_certificate_secure_transport_initable_interface_init))

static GBytes *
get_issuer_sequence (GTlsCertificateSecureTransport * self)
{
  GTlsCertificateSecureTransportPrivate *priv = g_tls_certificate_secure_transport_get_instance_private (self);
  CFDataRef issuer = NULL;
  GBytes *ret = NULL;

  issuer = SecCertificateCopyNormalizedIssuerSequence (priv->certificate);

  ret = g_bytes_new_with_free_func (CFDataGetBytePtr (issuer), CFDataGetLength (issuer),
      (GDestroyNotify) CFRelease, (gpointer) issuer);

  return ret;
}

static GBytes *
get_subject_sequence (GTlsCertificateSecureTransport * self)
{
  GTlsCertificateSecureTransportPrivate *priv = g_tls_certificate_secure_transport_get_instance_private (self);
  CFDataRef subject = NULL;
  GBytes *ret = NULL;

  subject = SecCertificateCopyNormalizedSubjectSequence (priv->certificate);

  ret = g_bytes_new_with_free_func (CFDataGetBytePtr (subject), CFDataGetLength (subject),
      (GDestroyNotify) CFRelease, (gpointer) subject);

  return ret;
}

static void
g_tls_certificate_secure_transport_import_private_key (GTlsCertificateSecureTransport * self, CFDataRef der)
{
  GTlsCertificateSecureTransportPrivate *priv = g_tls_certificate_secure_transport_get_instance_private (self);
  CFTypeRef keys[] = {kSecAttrKeyType, kSecAttrKeyClass};
  CFTypeRef values[] = {kSecAttrKeyTypeRSA, kSecAttrKeyClassPrivate};
  CFDictionaryRef options = CFDictionaryCreate (NULL, keys, values,
      G_N_ELEMENTS (keys), &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
  CFErrorRef error = NULL;
  SecKeyRef key = SecKeyCreateWithData(der, options, &error);

  if (!key) {
    /* XXX: deal with error */
    g_warn_if_reached ();
    return;
  } else {
    priv->private_key = key;
  }
  CFRelease (options);
}

static const char *
strip_prefix_suffix (const char * data, gsize len, const char * prefix, const char * suffix, gsize * out_len)
{
  char *p_begin, *p_end;

  p_begin = g_strstr_len (data, len, prefix);
  if (p_begin) {
    gsize start_index = (data - p_begin) + strlen (prefix);
    p_end = g_strstr_len (&p_begin[strlen(prefix)], len - start_index, suffix);
    if (p_end) {
      *out_len = p_end - p_begin - strlen (prefix);
      return p_begin + strlen(prefix);
    } else {
      *out_len = len;
      return data;
    }
  } else { 
    *out_len = len;
    return data;
  } 
}

static void
g_tls_certificate_secure_transport_set_property (GObject * obj, guint property_id, const GValue *value, GParamSpec *pspec)
{
  GTlsCertificateSecureTransport *self = G_TLS_CERTIFICATE_SECURE_TRANSPORT (obj);
  GTlsCertificateSecureTransportPrivate *priv = g_tls_certificate_secure_transport_get_instance_private (self);

  switch (property_id) {
    case PROP_CERTIFICATE: {
      GByteArray *bytes = g_value_get_boxed (value);

      if (priv->certificate) {
        CFRelease (priv->certificate);
        priv->certificate = NULL;
      }

      if (bytes) {
        CFDataRef data = CFDataCreate (NULL, bytes->data, bytes->len);
        priv->certificate = SecCertificateCreateWithData (NULL, data);
        g_warn_if_fail (priv->certificate);
        if (data)
          CFRelease (data);
      }
      break;
    }
    case PROP_CERTIFICATE_PEM: {
      const gchar *pem = g_value_get_string (value);
      gsize stripped_len = 0;
      const gchar *stripped;
      CFDataRef data;

      if (!pem)
        return;

      stripped = strip_prefix_suffix (pem, strlen(pem), CERT_PREFIX, CERT_SUFFIX, &stripped_len);

      if (!pem_to_der ((guint8 *) stripped, stripped_len, &data)) {
        g_warn_if_reached();
        return;
      }

      if (priv->certificate) {
        CFRelease (priv->certificate);
      }

      priv->certificate = SecCertificateCreateWithData (NULL, data);
      g_warn_if_fail (priv->certificate);

      g_free (priv->cert_pem);
      priv->cert_pem = g_strdup (pem);

      CFRelease (data);
      break;
    }
    case PROP_ISSUER: {
      if (priv->issuer)
        g_object_unref (priv->issuer);
      priv->issuer = g_value_dup_object (value);
      break;
    }
    case PROP_PRIVATE_KEY: {
      GByteArray *bytes = g_value_get_boxed (value);
      CFDataRef der = NULL;

      if (priv->private_key)
        return;

      g_assert (!bytes || priv->certificate);
      if (!bytes)
        return;

      der = CFDataCreate (NULL, bytes->data, bytes->len);
      g_tls_certificate_secure_transport_import_private_key (self, der);
      CFRelease (der);
      break;
    }
    case PROP_PRIVATE_KEY_PEM: {
      const gchar *pem = g_value_get_string (value);
      gsize stripped_len = 0;
      const gchar *stripped;
      CFDataRef der = NULL;

      if (priv->private_key)
        return;

      g_assert (!pem || priv->certificate);
      if (!pem)
        return;

      /* we need to strip the header and tail manually
       * https://stackoverflow.com/questions/10579985/how-can-i-get-seckeyref-from-der-pem-file
       */
      stripped = strip_prefix_suffix (pem, strlen(pem), KEY_PREFIX, KEY_SUFFIX, &stripped_len);

      if (!pem_to_der ((guint8 *) stripped, stripped_len, &der)) {
        g_warn_if_reached ();
        return;
      }

      g_tls_certificate_secure_transport_import_private_key (self, der);

      g_free (priv->key_pem);
      priv->key_pem = g_strdup (pem);

      CFRelease (der);
      break;
    }
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, property_id, pspec);
      break;
  }
}

static void
g_tls_certificate_secure_transport_get_property (GObject * obj, guint property_id, GValue *value, GParamSpec *pspec)
{
  GTlsCertificateSecureTransport *self = G_TLS_CERTIFICATE_SECURE_TRANSPORT (obj);
  GTlsCertificateSecureTransportPrivate *priv = g_tls_certificate_secure_transport_get_instance_private (self);

  switch (property_id) {
    case PROP_CERTIFICATE: {

      if (priv->certificate) {
        CFDataRef cert_data = SecCertificateCopyData(priv->certificate);
        GByteArray *bytes = g_byte_array_sized_new (CFDataGetLength(cert_data));

        g_byte_array_append (bytes, CFDataGetBytePtr(cert_data), CFDataGetLength(cert_data));

        g_value_take_boxed (value, bytes);
        if (cert_data)
          CFRelease (cert_data);
      }

      break;
    }
    case PROP_CERTIFICATE_PEM: {
      if (priv->cert_pem) {
       g_value_set_string (value, priv->cert_pem);
      } else if (priv->certificate) {
        CFDataRef cert_data = SecCertificateCopyData(priv->certificate);
        gchar *pem;

        /* Add the wrapping prefix/suffix */
        pem =  g_strconcat (CERT_PREFIX, (gchar *) der_to_pem (cert_data, NULL), CERT_SUFFIX, NULL);

        g_value_take_string (value, (gchar *) pem);
        if (cert_data)
          CFRelease (cert_data);
      }

      break;
    }
    case PROP_ISSUER: {
      g_value_set_object (value, priv->issuer);
      break;
    }
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, property_id, pspec);
      break;
  }
}

static void
g_tls_certificate_secure_transport_finalize (GObject * obj)
{
  GTlsCertificateSecureTransport *self = G_TLS_CERTIFICATE_SECURE_TRANSPORT (obj);
  GTlsCertificateSecureTransportPrivate *priv = g_tls_certificate_secure_transport_get_instance_private (self);

  if (priv->issuer) {
    g_object_unref (priv->issuer);
    priv->issuer = NULL;
  }

  if (priv->private_key) {
    CFRelease (priv->private_key);
    priv->private_key = NULL;
  }

  if (priv->certificate) {
    CFRelease (priv->certificate);
    priv->certificate = NULL;
  }

  g_free (priv->key_pem);
  priv->key_pem = NULL;

  g_free (priv->cert_pem);
  priv->cert_pem = NULL;

  G_OBJECT_CLASS (g_tls_certificate_secure_transport_parent_class)->finalize (obj);
}

CFMutableArrayRef
g_tls_certificate_secure_transport_to_cert_chain (GTlsCertificateSecureTransport * self)
{
  GTlsCertificate *iter = NULL;
  CFMutableArrayRef certs = NULL;

  g_return_val_if_fail (G_IS_TLS_CERTIFICATE_SECURE_TRANSPORT (self), NULL);

  certs = CFArrayCreateMutable (NULL, 0, &kCFTypeArrayCallBacks);
  for (iter = G_TLS_CERTIFICATE (self); iter; iter = g_tls_certificate_get_issuer (iter)) {
    SecCertificateRef cert_ref = g_tls_certificate_secure_transport_get_cert (G_TLS_CERTIFICATE_SECURE_TRANSPORT (iter));
    CFArrayAppendValue (certs, (CFTypeRef) cert_ref);
  }

  return certs;
}

GTlsCertificateFlags
g_tls_certificate_secure_transport_verify_internal (GTlsCertificateSecureTransport *self,
    GSocketConnectable *identity, const gchar * purpose, GList * trusted_cas)
{
  GTlsCertificateFlags ret = G_TLS_CERTIFICATE_VALIDATE_ALL;
  SecTrustRef trust = NULL;
  SecPolicyRef policy = NULL;
  CFStringRef cserver_name = NULL;
  CFMutableArrayRef certs, anchor_certs;
  OSStatus status;
  GList *l = NULL;

  certs = g_tls_certificate_secure_transport_to_cert_chain (self);

  anchor_certs = CFArrayCreateMutable (NULL, 0, &kCFTypeArrayCallBacks);
  for (l = trusted_cas; l; l = g_list_next (l)) {
    GTlsCertificateSecureTransport *ca = G_TLS_CERTIFICATE_SECURE_TRANSPORT (l->data);
    SecCertificateRef anchor_cert = g_tls_certificate_secure_transport_get_cert (ca);
    CFArrayAppendValue (anchor_certs, (CFTypeRef) anchor_cert);
  }

  if (identity) {
    gchar *server_name = NULL;

    server_name = g_tls_secure_transport_socket_connectable_to_string (identity);
    cserver_name = CFStringCreateWithCString (NULL, server_name, kCFStringEncodingUTF8);
    g_free (server_name);
  }

  policy = SecPolicyCreateSSL(g_strcmp0(purpose, G_TLS_DATABASE_PURPOSE_AUTHENTICATE_SERVER) == 0, cserver_name);
  if (!policy) {
    ret = G_TLS_CERTIFICATE_GENERIC_ERROR;
    g_warn_if_reached ();
    goto out;
  }

  status = SecTrustCreateWithCertificates ((CFTypeRef) certs, (CFTypeRef) policy, &trust);
  if (!trust || !warn_osstatus(status)) {
    ret = G_TLS_CERTIFICATE_GENERIC_ERROR;
    goto out;
  }

  status = SecTrustSetAnchorCertificates (trust, (CFTypeRef) anchor_certs);
  if (!warn_osstatus(status)) {
    ret = G_TLS_CERTIFICATE_GENERIC_ERROR;
    goto out;
 }

  ret = secure_transport_trust_evaluate (trust);

out:
  if (certs)
    CFRelease (certs);
  if (anchor_certs)
    CFRelease (anchor_certs);
  if (trust)
    CFRelease (trust);
  if (policy)
    CFRelease (policy);
  if (cserver_name)
    CFRelease (cserver_name);

  return ret;

}

static GTlsCertificateFlags
g_tls_certificate_secure_transport_verify (GTlsCertificate *cert, GSocketConnectable *identity, GTlsCertificate *trusted_ca)
{
  GTlsCertificateSecureTransport *self = G_TLS_CERTIFICATE_SECURE_TRANSPORT (cert);
  GTlsCertificateFlags ret = 0;
  GList *cas = NULL;

  if (trusted_ca)
    cas = g_list_prepend (cas, trusted_ca);

  ret = g_tls_certificate_secure_transport_verify_internal (self, identity, G_TLS_DATABASE_PURPOSE_AUTHENTICATE_SERVER, cas);

  g_list_free (cas);

  return ret;
}

static void
g_tls_certificate_secure_transport_class_init (GTlsCertificateSecureTransportClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;
  GTlsCertificateClass *certificate_class = G_TLS_CERTIFICATE_CLASS (klass);

  gobject_class->set_property = g_tls_certificate_secure_transport_set_property;
  gobject_class->get_property = g_tls_certificate_secure_transport_get_property;
  gobject_class->finalize = g_tls_certificate_secure_transport_finalize;

  g_object_class_override_property (gobject_class, PROP_CERTIFICATE, "certificate");
  g_object_class_override_property (gobject_class, PROP_CERTIFICATE_PEM, "certificate-pem");
  g_object_class_override_property (gobject_class, PROP_ISSUER, "issuer");
  g_object_class_override_property (gobject_class, PROP_PRIVATE_KEY, "private-key");
  g_object_class_override_property (gobject_class, PROP_PRIVATE_KEY_PEM, "private-key-pem");

  certificate_class->verify = g_tls_certificate_secure_transport_verify;
}

static gboolean
g_tls_certificate_secure_transport_initable_init (GInitable *initable, GCancellable *cancellable, GError **error)
{
  return TRUE;
}

static void
g_tls_certificate_secure_transport_initable_interface_init (GInitableIface *iface)
{
  iface->init = g_tls_certificate_secure_transport_initable_init;
}

static void
g_tls_certificate_secure_transport_init (GTlsCertificateSecureTransport *self)
{
}

GTlsCertificate *
g_tls_certificate_secure_transport_new (GBytes * bytes, GTlsCertificate *issuer)
{
  GByteArray *arr = g_byte_array_sized_new (g_bytes_get_size (bytes));
  GTlsCertificate *ret;

  g_byte_array_append (arr, g_bytes_get_data (bytes, NULL), g_bytes_get_size (bytes));

  ret = g_object_new (G_TYPE_TLS_CERTIFICATE_SECURE_TRANSPORT,
        "issuer", issuer, "certificate", bytes, NULL);
  g_byte_array_unref (arr);

  return ret;
}

GTlsCertificate *
g_tls_certificate_secure_transport_new_with_cert (SecCertificateRef cert_ref, GTlsCertificate *issuer)
{
  GTlsCertificate *ret;
  GTlsCertificateSecureTransportPrivate *priv;

  g_return_val_if_fail (cert_ref != NULL, NULL);

  ret = g_object_new (G_TYPE_TLS_CERTIFICATE_SECURE_TRANSPORT,
        "issuer", issuer, NULL);
  priv = g_tls_certificate_secure_transport_get_instance_private (G_TLS_CERTIFICATE_SECURE_TRANSPORT (ret));
  priv->certificate = (SecCertificateRef) CFRetain (cert_ref);

  return ret;
}

SecCertificateRef
g_tls_certificate_secure_transport_get_cert (GTlsCertificateSecureTransport *self)
{
  GTlsCertificateSecureTransportPrivate *priv;

  g_return_val_if_fail (G_IS_TLS_CERTIFICATE_SECURE_TRANSPORT (self), NULL);

  priv = g_tls_certificate_secure_transport_get_instance_private (self);

  return priv->certificate;
}

SecKeyRef
g_tls_certificate_secure_transport_get_key (GTlsCertificateSecureTransport *self)
{
  GTlsCertificateSecureTransportPrivate *priv;

  g_return_val_if_fail (G_IS_TLS_CERTIFICATE_SECURE_TRANSPORT (self), NULL);

  priv = g_tls_certificate_secure_transport_get_instance_private (self);

  return priv->private_key;
}

GBytes *
g_tls_certificate_secure_transport_get_cert_bytes (GTlsCertificateSecureTransport *self)
{
  GByteArray *array;

  g_return_val_if_fail (G_IS_TLS_CERTIFICATE_SECURE_TRANSPORT (self), NULL);

  g_object_get (self, "certificate", &array, NULL);
  return g_byte_array_free_to_bytes (array);
}

GBytes *
g_tls_certificate_secure_transport_get_issuer_sequence (GTlsCertificateSecureTransport *self)
{
  g_return_val_if_fail (G_IS_TLS_CERTIFICATE_SECURE_TRANSPORT (self), NULL);

  return get_issuer_sequence (self);;
}

GBytes *
g_tls_certificate_secure_transport_get_subject_sequence (GTlsCertificateSecureTransport *self)
{
  g_return_val_if_fail (G_IS_TLS_CERTIFICATE_SECURE_TRANSPORT (self), NULL);

  return get_subject_sequence (self);
}
