/*
 * gtlsconnection-securetransport.c
 *
 * Copyright (C) 2018 Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "config.h"

#include "gtlsconnection-securetransport.h"
#include "gtlscertificate-securetransport.h"
#include "gtlsutils-securetransport.h"

typedef struct _GTlsConnectionSecureTransportPrivate {
  GTlsCertificate *peer_certificate;
  GTlsCertificateFlags peer_certificate_errors;
} GTlsConnectionSecureTransportPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_CODE (GTlsConnectionSecureTransport, g_tls_connection_secure_transport, G_TYPE_TLS_CONNECTION_BASE,
                         G_ADD_PRIVATE (GTlsConnectionSecureTransport))

/* XXX: this is private macOS/iOS API
 * Not using this involves creating a PKCS#12 blob and using SecPKCS12Import() */
extern SecIdentityRef
SecIdentityCreate(
	CFAllocatorRef allocator,
	SecCertificateRef certificate,
	SecKeyRef privateKey);

static void
g_tls_connection_secure_transport_finalize (GObject * obj)
{
  GTlsConnectionSecureTransport *self = G_TLS_CONNECTION_SECURE_TRANSPORT (obj);
  GTlsConnectionSecureTransportPrivate *priv = g_tls_connection_secure_transport_get_instance_private (self);

  if (self->context)
    CFRelease (self->context);
  self->context = NULL;

  if (priv->peer_certificate) {
    g_object_unref (priv->peer_certificate);
    priv->peer_certificate = NULL;
  }

  G_OBJECT_CLASS (g_tls_connection_secure_transport_parent_class)->finalize (obj);
}

static OSStatus
on_ssl_read (SSLConnectionRef connection, void *data, size_t *dataLength)
{
  GTlsConnectionSecureTransport *self = G_TLS_CONNECTION_SECURE_TRANSPORT (connection);
  GTlsConnectionBase *tls = G_TLS_CONNECTION_BASE (connection);
  OSStatus ret = errSecSuccess;
  gboolean handshaking = tls->handshaking;
  GCancellable *cancellable = NULL;
  gboolean blocking = FALSE;
  GError **error_ptr = NULL;
  GError *error = NULL;
  gsize n_read = 0;

  if (handshaking || self->shutting_down) {
    blocking = TRUE;
    cancellable = NULL;
    error_ptr = &error;
  } else {
    blocking = tls->read_blocking;
    cancellable = tls->read_cancellable;
    error_ptr = &tls->read_error;
  }

  while (n_read < *dataLength) {
    gssize received = g_pollable_stream_read (g_io_stream_get_input_stream (tls->base_io_stream),
                                     (char *)data + n_read, *dataLength - n_read, blocking, NULL,
                                     error_ptr);
    if (received < 0)
      break;
    n_read += received;
  }

  g_debug ("%p ssl_read, attempted %" G_GSIZE_FORMAT " nread %" G_GSSIZE_FORMAT,
      tls, *dataLength, n_read);

  if (n_read <= 0) {
    if (g_error_matches (*error_ptr, G_IO_ERROR, G_IO_ERROR_WOULD_BLOCK)) {
      ret = errSSLWouldBlock;
    } else if (g_error_matches (*error_ptr, G_IO_ERROR, G_IO_ERROR_CONNECTION_CLOSED)) {
      ret = errSSLClosedAbort;
    } else {
      /* TODO: better errors? */
      ret = errSecIO;
    }
  }

  *dataLength = MAX (0, n_read);
  return ret;
}

static OSStatus
on_ssl_write (SSLConnectionRef connection, const void *data, size_t *dataLength)
{
  GTlsConnectionSecureTransport *self = G_TLS_CONNECTION_SECURE_TRANSPORT (connection);
  GTlsConnectionBase *tls = G_TLS_CONNECTION_BASE (connection);
  OSStatus ret = errSecSuccess;
  gboolean handshaking = tls->handshaking;
  GCancellable *cancellable = NULL;
  gboolean blocking = FALSE;
  GError **error_ptr = NULL;
  GError *error = NULL;
  gsize n_written = 0;
  gboolean success;

  if (handshaking || self->shutting_down) {
    blocking = TRUE;
    cancellable = NULL;
    error_ptr = &error;
  } else {
    blocking = tls->write_blocking;
    cancellable = tls->write_cancellable;
    error_ptr = &tls->write_error;
  }

  success = g_pollable_stream_write_all (g_io_stream_get_output_stream (tls->base_io_stream),
                                         data, *dataLength, blocking, &n_written,
                                         NULL, error_ptr);

  g_debug ("%p ssl_write, attempted %" G_GSIZE_FORMAT " nwrote %" G_GSIZE_FORMAT,
      tls, *dataLength, n_written);
  if (n_written < *dataLength) {
    ret = errSSLWouldBlock;
  }

  if (!success) {
    if (g_error_matches (*error_ptr, G_IO_ERROR, G_IO_ERROR_WOULD_BLOCK)) {
      ret = errSSLWouldBlock;
    } else if (g_error_matches (*error_ptr, G_IO_ERROR, G_IO_ERROR_CONNECTION_CLOSED)) {
      ret = errSSLClosedAbort;
    } else {
      /* TODO: better errors? */
      ret = errSecIO;
    }
  }
  *dataLength = MIN (*dataLength, n_written);

  return ret;
}

static OSStatus
_setup_ssl_context (GTlsConnectionSecureTransport * self)
{
  OSStatus status = errSecSuccess;

  if ((status = SSLSetConnection (self->context, self)) != errSecSuccess)
    return status;
  if ((status = SSLSetIOFuncs (self->context, on_ssl_read, on_ssl_write)) != errSecSuccess)
    return status;
  if ((status = SSLSetSessionOption (self->context, kSSLSessionOptionBreakOnServerAuth, TRUE)) != errSecSuccess)
    return status;
  if ((status = SSLSetSessionOption (self->context, kSSLSessionOptionBreakOnClientAuth, TRUE)) != errSecSuccess)
    return status;
  if (G_IS_TLS_CLIENT_CONNECTION (self) && self->authentication_mode != G_TLS_AUTHENTICATION_NONE) {
    if ((status = SSLSetSessionOption (self->context, kSSLSessionOptionBreakOnCertRequested, TRUE)) != errSecSuccess)
      return status;
  }
  /* FIXME: align renegotiation and G_TLS_REHANDSHAKE_MODE */
  if ((status = SSLSetSessionOption (self->context, kSSLSessionOptionAllowRenegotiation, TRUE)) != errSecSuccess)
    return status;

  return status;
}

static GTlsConnectionBaseStatus
g_tls_connection_secure_transport_request_rehandshake (GTlsConnectionBase *tls, GCancellable *cancellable, GError **error)
{
  GTlsConnectionSecureTransport *self = G_TLS_CONNECTION_SECURE_TRANSPORT (tls);
  OSStatus status;

  g_debug ("%p rehandshake", tls);
  status = SSLReHandshake (self->context);
  if (status != errSecSuccess) {
    gchar *msg = osstatus_to_string (status);
    g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC, "%s", msg);
    g_free (msg);
    return G_TLS_CONNECTION_BASE_ERROR;
  }

  return G_TLS_CONNECTION_BASE_OK;
}

static GTlsConnectionBaseStatus
set_cert (GTlsConnectionSecureTransport * self, GTlsCertificate *cert, GError **error)
{
  GTlsCertificateSecureTransport *secure_cert = G_TLS_CERTIFICATE_SECURE_TRANSPORT (cert);
  GTlsCertificate *issuer;
  SecIdentityRef identity;
  SecCertificateRef cert_ref;
  SecKeyRef key_ref;
  CFMutableArrayRef cert_chain;

  cert_ref = g_tls_certificate_secure_transport_get_cert (secure_cert);
  key_ref = g_tls_certificate_secure_transport_get_key (secure_cert);

  if (!cert_ref) {
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_CERTIFICATE_REQUIRED,
        "Could not get internal server certificate");
    return G_TLS_CONNECTION_BASE_ERROR;
  }
  if (!key_ref) {
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_CERTIFICATE_REQUIRED,
        "No private key provided in certificate");
    return G_TLS_CONNECTION_BASE_ERROR;
  }

  /* FIXME: this is private API and grounds for rejection from the app store */
  identity = SecIdentityCreate (NULL, cert_ref, key_ref);

  cert_chain = CFArrayCreateMutable (NULL, 0, &kCFTypeArrayCallBacks);
  CFArrayAppendValue (cert_chain, identity);
  issuer = g_tls_certificate_get_issuer (cert);
  while (issuer) {
    GTlsCertificateSecureTransport *secure_cert_iter = G_TLS_CERTIFICATE_SECURE_TRANSPORT (issuer);
    cert_ref = g_tls_certificate_secure_transport_get_cert (secure_cert_iter);
    CFArrayAppendValue (cert_chain, cert_ref);
    issuer = g_tls_certificate_get_issuer (issuer);
  }
  SSLSetCertificate (self->context, cert_chain);
  CFRelease (cert_chain);

  return G_TLS_CONNECTION_BASE_OK;
}

static GTlsConnectionBaseStatus
_auth_peer (GTlsConnectionSecureTransport * self, GCancellable * cancellable, GError ** error)
{
  GTlsConnectionSecureTransportPrivate *priv = g_tls_connection_secure_transport_get_instance_private (self);
  GSocketConnectable *identity = NULL;
  GTlsCertificateFlags cert_flags = 0;
  GTlsCertificate * cert = NULL;
  GTlsDatabase *database;
  OSStatus status;
  SecTrustRef trust;
  int i;

  /* Get the peer certificate, if any, and validate it */
  if (G_IS_TLS_CLIENT_CONNECTION (self))
    identity = g_tls_client_connection_get_server_identity (G_TLS_CLIENT_CONNECTION (self));
  database = g_tls_connection_get_database (G_TLS_CONNECTION (self));

  status = SSLCopyPeerTrust (self->context, &trust);
  if (status != errSecSuccess || !trust) {
    if (self->authentication_mode == G_TLS_AUTHENTICATION_REQUIRED) {
      g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_CERTIFICATE_REQUIRED,
          "Peer did not return a valid TLS certificate");
      g_debug ("%p peer auth failed", self);
      return G_TLS_CONNECTION_BASE_ERROR;
    } else {
      g_debug ("%p peer auth failed (but not required, continuing)", self);
      return G_TLS_CONNECTION_BASE_OK;
    }
  }

  /* two ways to do this are to get the peer trust and set the anchor certs
   * from the database on it, or get the cert chain from the trust and use the
   * existing verify functions which creates a new trust.
   *
   * We current use the latter */
  for (i = SecTrustGetCertificateCount (trust) - 1; i >= 0; i--) {
    SecCertificateRef cert_ref = SecTrustGetCertificateAtIndex (trust, i);
    GTlsCertificate *prev_cert = cert;

    cert = g_tls_certificate_secure_transport_new_with_cert (cert_ref, prev_cert);

    if (prev_cert)
      g_object_unref (prev_cert);
  }

  /* Verify chain */
  if (!database) {
    cert_flags |= G_TLS_CERTIFICATE_UNKNOWN_CA;
    cert_flags |= g_tls_certificate_verify (cert, identity, NULL);
  } else {
    cert_flags |= g_tls_database_verify_chain (database, cert,
                                               G_IS_TLS_CLIENT_CONNECTION (self) ?
                                                  G_TLS_DATABASE_PURPOSE_AUTHENTICATE_SERVER :
                                                  G_TLS_DATABASE_PURPOSE_AUTHENTICATE_CLIENT,
                                               identity,
                                               g_tls_connection_get_interaction (G_TLS_CONNECTION (self)),
                                               G_TLS_DATABASE_VERIFY_NONE,
                                               NULL, NULL);
  }

  g_debug ("%p processed peer certificate. result flags: %x", self, cert_flags);

  if (priv->peer_certificate)
    g_object_unref (priv->peer_certificate);
  priv->peer_certificate = cert;
  priv->peer_certificate_errors = cert_flags;

  return G_TLS_CONNECTION_BASE_OK;
}

static void
_retrieve_cas (GTlsConnectionSecureTransport * self)
{
  GTlsConnectionSecureTransportClass *klass;

  klass = G_TLS_CONNECTION_SECURE_TRANSPORT_GET_CLASS (self);

  if (klass->retrieve_cas)
    klass->retrieve_cas (self);
}

static const gchar *
base_status_to_string (GTlsConnectionBaseStatus ret)
{
  switch (ret) {
    case G_TLS_CONNECTION_BASE_WOULD_BLOCK:
      return "would-block";
    case G_TLS_CONNECTION_BASE_ERROR:
      return "error";
    case G_TLS_CONNECTION_BASE_OK:
      return "ok";
    default:
      return "unknown";
  }
}

static GTlsConnectionBaseStatus
osstatus_to_gtls (GTlsConnectionSecureTransport * self, OSStatus status, GError ** error)
{
  GTlsConnectionBaseStatus ret = G_TLS_CONNECTION_BASE_OK;

  if (status == errSSLWouldBlock) {
    g_set_error_literal (error, G_IO_ERROR, G_IO_ERROR_WOULD_BLOCK,
        "IO would block (non-fatal)");
    ret = G_TLS_CONNECTION_BASE_WOULD_BLOCK;
  } else if (status == errSSLClosedAbort) {
    if (!self->shutting_down) {
      g_set_error_literal (error, G_IO_ERROR, G_IO_ERROR_CONNECTION_CLOSED,
          "Connection closed abruptly");
      ret = G_TLS_CONNECTION_BASE_ERROR;
    }
  } else if (status == errSSLClosedGraceful) {
    /* nothing to do */
  } else if (status != errSecSuccess) {
    char *msg = osstatus_to_string (status);
    g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC, "%s", msg);
    g_free (msg);
    ret = G_TLS_CONNECTION_BASE_ERROR;
  }

  g_debug ("%p status: %i result: %s", self, (int) status, base_status_to_string (ret));

  return ret;
}

static GTlsConnectionBaseStatus
g_tls_connection_secure_transport_close (GTlsConnectionBase *tls, GCancellable *cancellable, GError **error)
{
  GTlsConnectionSecureTransport *self = G_TLS_CONNECTION_SECURE_TRANSPORT (tls);
  OSStatus status;

  g_debug ("%p close requested", self);

  self->shutting_down = TRUE;
  status = SSLClose (self->context);
  switch (status) {
    case errSecSuccess:
    case errSSLClosedGraceful:
    case errSSLClosedAbort:
      status = errSecSuccess;
      break;
    default: {
      char *msg = osstatus_to_string (status);
      g_debug ("%p close: %s", tls, msg);
      g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC, "%s", msg);
      return G_TLS_CONNECTION_BASE_ERROR;
    }
  }

  return G_TLS_CONNECTION_BASE_OK;
}

static GTlsConnectionBaseStatus
g_tls_connection_secure_transport_handshake (GTlsConnectionBase *tls, GCancellable *cancellable, GError **error)
{
  GTlsConnectionSecureTransport *self = G_TLS_CONNECTION_SECURE_TRANSPORT (tls);
  GTlsConnectionBaseStatus ret = G_TLS_CONNECTION_BASE_OK;
  GTlsCertificate *cert = NULL;
  OSStatus status = errSecSuccess;
  gboolean handshake_started = FALSE;

  if (!tls->ever_handshaked) {
    GSocketConnectable *identity = NULL;

    if (G_IS_TLS_CLIENT_CONNECTION (self))
      identity = g_tls_client_connection_get_server_identity (G_TLS_CLIENT_CONNECTION (self));

    if (identity) {
      gchar *server_name = NULL;

      server_name = g_tls_secure_transport_socket_connectable_to_string (identity);

      if (server_name)
        status = SSLSetPeerDomainName (self->context, server_name, strlen (server_name));

      g_free (server_name);
    }

    if (status == errSecSuccess)
      status = _setup_ssl_context (self);
  }

  if (status != errSecSuccess) {
    char *msg = osstatus_to_string (status);
    g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC, "%s", msg);
    g_free (msg);
    return G_TLS_CONNECTION_BASE_ERROR;
  }

  /* only set the cert before handshake with server connections
   * otherwise the client-auth tests get confused as we won't
   * request a certificate because the cert has already been set.
   * FIXME: fix the tests? */
  if (G_IS_TLS_SERVER_CONNECTION (tls)) {
new_certificate:
    cert = g_tls_connection_get_certificate (G_TLS_CONNECTION (tls));
    if (!cert && G_IS_TLS_SERVER_CONNECTION (tls)) {
      g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_CERTIFICATE_REQUIRED,
          "No %s certificate provided",
          G_IS_TLS_SERVER_CONNECTION (tls) ? "server" : "client");
      return G_TLS_CONNECTION_BASE_ERROR;
    }
  }

  if (cert) {
    if ((ret = set_cert (self, cert, error)) != G_TLS_CONNECTION_BASE_OK) {
      goto close_and_ret;
    }
  }

handshake:
  handshake_started = TRUE;
  status = SSLHandshake (self->context);
  {
    char *msg = osstatus_to_string (status);
    g_debug ("%p handshake: %i: %s", self, (int) status, msg);
    g_free (msg);
  }
  if (status == errSSLClientCertRequested) {
    GTlsInteraction *interaction;

    g_assert (G_IS_TLS_CLIENT_CONNECTION (self));

    _retrieve_cas (self);
    if ((interaction = g_tls_connection_get_interaction (G_TLS_CONNECTION (self)))) {
      GTlsInteractionResult res;

      res = g_tls_interaction_invoke_request_certificate (interaction, G_TLS_CONNECTION (self), 0,
                                                            G_TLS_CONNECTION_BASE (self)->read_cancellable, error);
      if (res == G_TLS_INTERACTION_FAILED) {
        ret = G_TLS_CONNECTION_BASE_ERROR;
        goto close_and_ret;
      }
    }
    /* Need to get new credentials now that include the certificate */
    goto new_certificate;
  } else if (status == errSSLPeerAuthCompleted) {
    if ((ret = _auth_peer (self, cancellable, error)) == G_TLS_CONNECTION_BASE_OK) {
      goto handshake;
    } else {
      goto close_and_ret;
    }
  } else if (status == errSSLClosedAbort) {
    ret = osstatus_to_gtls (self, status, error);
    goto close_and_ret;
  }

  return osstatus_to_gtls (self, status, error);

close_and_ret:
  if (handshake_started) {
    /* This close is needed on failure to mitigate a deadlock in the parent class.
     * Error's aren't really useful as we've already hit one */
    g_tls_connection_secure_transport_close (tls, cancellable, NULL);
  }

  return ret;
}

static GTlsConnectionBaseStatus
g_tls_connection_secure_transport_complete_handshake (GTlsConnectionBase *tls, GError **error)
{
  GTlsConnectionSecureTransport *self = G_TLS_CONNECTION_SECURE_TRANSPORT (tls);
  GTlsConnectionSecureTransportPrivate *priv = g_tls_connection_secure_transport_get_instance_private (self);
  GTlsConnectionBaseStatus ret = G_TLS_CONNECTION_BASE_OK;

  if (priv->peer_certificate) {
    if (!g_tls_connection_base_accept_peer_certificate (G_TLS_CONNECTION_BASE (self), priv->peer_certificate, priv->peer_certificate_errors)) {
      g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_BAD_CERTIFICATE,
          "Unacceptable certificate");
      ret = G_TLS_CONNECTION_BASE_ERROR;
    } else {
      g_tls_connection_base_set_peer_certificate (G_TLS_CONNECTION_BASE (self), priv->peer_certificate, priv->peer_certificate_errors);
      ret = G_TLS_CONNECTION_BASE_OK;
    }
  }

  priv->peer_certificate = NULL;
  priv->peer_certificate_errors = 0;

  return G_TLS_CONNECTION_BASE_OK;
}

static gboolean
io_success_values (OSStatus status)
{
  switch (status) {
    case errSecSuccess:
    case errSSLClientCertRequested:
    case errSSLPeerAuthCompleted:
    case errSSLClosedGraceful:
    case errSSLClosedAbort:
      return TRUE;
    default:
      return FALSE;
  }
}

static GTlsConnectionBaseStatus
g_tls_connection_secure_transport_read (GTlsConnectionBase *tls, void *buffer, gsize count, gboolean blocking, gssize *nread,
                                GCancellable *cancellable, GError **error)
{
  GTlsConnectionSecureTransport *self = G_TLS_CONNECTION_SECURE_TRANSPORT (tls);
  GTlsConnectionBaseStatus ret;
  OSStatus status;

  if (count == 0)
    return G_TLS_CONNECTION_BASE_OK;

  g_tls_connection_base_push_io (tls, G_IO_IN, blocking, cancellable);
  status = SSLRead (self->context, buffer, count, (gsize *) nread);
  {
    char *msg = osstatus_to_string (status);
    g_debug ("%p read: %i: %s", tls, (int) status, msg);
    g_free (msg);
  }

  ret = g_tls_connection_base_pop_io (tls, G_IO_IN, io_success_values (status), error);
  if (ret != G_TLS_CONNECTION_BASE_OK)
    goto out;

  switch (status) {
    case errSSLClientCertRequested:
    case errSSLPeerAuthCompleted:
      g_assert (*nread == 0);
      return G_TLS_CONNECTION_BASE_REHANDSHAKE;
    default:
      break;
  }
  ret = osstatus_to_gtls (self, status, error);

out:
  g_debug ("%p read returning %u, %p status %i", self, ret, error ? *error : NULL, (int) status);
  return ret;
}

static GTlsConnectionBaseStatus
g_tls_connection_secure_transport_write (GTlsConnectionBase *tls, const void *buffer, gsize count, gboolean blocking,
                                 gssize *nwrote, GCancellable *cancellable, GError **error)
{
  GTlsConnectionSecureTransport *self = G_TLS_CONNECTION_SECURE_TRANSPORT (tls);
  GTlsConnectionBaseStatus ret;
  OSStatus status;

  g_tls_connection_base_push_io (tls, G_IO_OUT, blocking, cancellable);
  status = SSLWrite (self->context, buffer, count, (gsize *) nwrote);
  {
    char *msg = osstatus_to_string (status);
    g_debug ("%p write: %i: %s", tls, (int) status, msg);
    g_free (msg);
  }

  ret = g_tls_connection_base_pop_io (tls, G_IO_OUT, io_success_values (status), error);
  if (ret != G_TLS_CONNECTION_BASE_OK)
    goto out;

  switch (status) {
    case errSSLClientCertRequested:
    case errSSLPeerAuthCompleted:
      g_assert (*nwrote == 0);
      return G_TLS_CONNECTION_BASE_REHANDSHAKE;
    default:
      break;
  }

  ret = osstatus_to_gtls (self, status, error);

out:
  g_debug ("%p write returning %u, %p status %i", self, ret, error ? *error : NULL, (int) status);
  return ret;
}

static void
g_tls_connection_secure_transport_class_init (GTlsConnectionSecureTransportClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;
  GTlsConnectionBaseClass *base_connection_class = G_TLS_CONNECTION_BASE_CLASS (klass);

  gobject_class->finalize = g_tls_connection_secure_transport_finalize;

  base_connection_class->request_rehandshake = g_tls_connection_secure_transport_request_rehandshake;
  base_connection_class->handshake           = g_tls_connection_secure_transport_handshake;
  base_connection_class->complete_handshake  = g_tls_connection_secure_transport_complete_handshake;
  base_connection_class->read_fn             = g_tls_connection_secure_transport_read;
  base_connection_class->write_fn            = g_tls_connection_secure_transport_write;
  base_connection_class->close_fn            = g_tls_connection_secure_transport_close;
}

static void
g_tls_connection_secure_transport_init (GTlsConnectionSecureTransport *self)
{
  self->authentication_mode = G_TLS_AUTHENTICATION_REQUIRED;
}
