/*
 * gtlsclientconnection-securetransport.c
 *
 * Copyright (C) 2018 Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "config.h"

#include "gtlsclientconnection-securetransport.h"
#include "gtlscertificate-securetransport.h"
#include "gtlsutils-securetransport.h"

#include <Security/Security.h>

enum {
  PROP_ACCEPTED_CAS = 1,
  PROP_SERVER_IDENTITY,
  PROP_USE_SSL3,
  PROP_VALIDATION_FLAGS
};

struct _GTlsClientConnectionSecureTransport {
  GTlsConnectionSecureTransport parent;
};

typedef struct _GTlsClientConnectionSecureTransportPrivate {
  GSocketConnectable *server_identity;
  gboolean use_ssl3;
  GTlsCertificateFlags validation_flags;
  GList *accepted_cas;
} GTlsClientConnectionSecureTransportPrivate;

static void g_tls_client_connection_secure_transport_client_connection_interface_init (GTlsClientConnectionInterface *iface);
static void g_tls_client_connection_secure_transport_initable_interface_init (GInitableIface *iface);

G_DEFINE_TYPE_WITH_CODE (GTlsClientConnectionSecureTransport, g_tls_client_connection_secure_transport, G_TYPE_TLS_CONNECTION_SECURE_TRANSPORT,
                         G_ADD_PRIVATE (GTlsClientConnectionSecureTransport)
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE, g_tls_client_connection_secure_transport_initable_interface_init)
                         G_IMPLEMENT_INTERFACE (G_TYPE_TLS_CLIENT_CONNECTION, g_tls_client_connection_secure_transport_client_connection_interface_init))

static void
g_tls_client_connection_secure_transport_set_property (GObject * obj, guint property_id, const GValue *value, GParamSpec *pspec)
{
  GTlsClientConnectionSecureTransport *self = G_TLS_CLIENT_CONNECTION_SECURE_TRANSPORT (obj);
  GTlsClientConnectionSecureTransportPrivate *priv = g_tls_client_connection_secure_transport_get_instance_private (self);

  switch (property_id) {
    case PROP_SERVER_IDENTITY:
      if (priv->server_identity)
        g_object_unref (priv->server_identity);
      priv->server_identity = g_value_dup_object (value);
      break;
    case PROP_USE_SSL3:
      priv->use_ssl3 = g_value_get_boolean (value);
      break;
    case PROP_VALIDATION_FLAGS:
      priv->validation_flags = g_value_get_flags (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, property_id, pspec);
      break;
  }
}

static void
g_tls_client_connection_secure_transport_get_property (GObject * obj, guint property_id, GValue *value, GParamSpec *pspec)
{
  GTlsClientConnectionSecureTransport *self = G_TLS_CLIENT_CONNECTION_SECURE_TRANSPORT (obj);
  GTlsClientConnectionSecureTransportPrivate *priv = g_tls_client_connection_secure_transport_get_instance_private (self);

  switch (property_id) {
    case PROP_ACCEPTED_CAS: {
      g_value_set_pointer (value, g_list_copy_deep (priv->accepted_cas, (GCopyFunc) g_byte_array_ref, NULL));
      break;
    }
    case PROP_SERVER_IDENTITY:
      g_value_set_object (value, priv->server_identity);
      break;
    case PROP_USE_SSL3:
      g_value_set_boolean (value, priv->use_ssl3);
      break;
    case PROP_VALIDATION_FLAGS:
      g_value_set_flags (value, priv->validation_flags);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, property_id, pspec);
      break;
  }
}

static void
g_tls_client_connection_secure_transport_finalize (GObject * obj)
{
  GTlsClientConnectionSecureTransport *self = G_TLS_CLIENT_CONNECTION_SECURE_TRANSPORT (obj);
  GTlsClientConnectionSecureTransportPrivate *priv = g_tls_client_connection_secure_transport_get_instance_private (self);

  g_list_free_full (priv->accepted_cas, (GDestroyNotify) g_byte_array_unref);
  priv->accepted_cas = NULL;

  G_OBJECT_CLASS (g_tls_client_connection_secure_transport_parent_class)->finalize (obj);
}

static void
g_tls_client_connection_secure_transport_retrieve_cas (GTlsConnectionSecureTransport * secure)
{
  GTlsClientConnectionSecureTransport *self = G_TLS_CLIENT_CONNECTION_SECURE_TRANSPORT (secure);
  GTlsClientConnectionSecureTransportPrivate *priv;
  GQueue accepted_cas = G_QUEUE_INIT;
  OSStatus status;
  CFArrayRef dns;
  int i;

  priv = g_tls_client_connection_secure_transport_get_instance_private (self);

  g_list_free_full (priv->accepted_cas, (GDestroyNotify) g_byte_array_unref);
  priv->accepted_cas = NULL;

  status = SSLCopyDistinguishedNames (secure->context, &dns);
  if (!warn_osstatus (status)) {
    return;
  }

  if (!dns)
    goto out;

  for (i = 0; i < CFArrayGetCount (dns); i++) {
    CFDataRef dn = (CFDataRef) CFArrayGetValueAtIndex (dns, i);
    GByteArray *arr;

    arr = g_byte_array_sized_new (CFDataGetLength (dn));
    g_byte_array_append (arr, CFDataGetBytePtr (dn), CFDataGetLength( dn));
    g_queue_push_tail (&accepted_cas, arr);
  }

  CFRelease (dns);

  priv->accepted_cas = accepted_cas.head;
out:
  g_object_notify (G_OBJECT (self), "accepted-cas");
}

static GTlsConnectionBaseStatus
g_tls_client_connection_secure_transport_handshake (GTlsConnectionBase *tls, GCancellable *cancellable, GError **error)
{
  GTlsClientConnectionSecureTransport *self = G_TLS_CLIENT_CONNECTION_SECURE_TRANSPORT (tls);
  GTlsConnectionSecureTransport *base = G_TLS_CONNECTION_SECURE_TRANSPORT (self);

  if (!base->context) {
    base->context = SSLCreateContext(NULL, kSSLClientSide, kSSLStreamType);
  }

  return G_TLS_CONNECTION_BASE_CLASS (g_tls_client_connection_secure_transport_parent_class)->handshake (tls, cancellable, error);
}

static void
g_tls_client_connection_secure_transport_copy_session_state (GTlsClientConnection *tls, GTlsClientConnection *source)
{
  /* TODO: Not clear how this is supposed to work */
  g_warn_if_reached ();
}

static void
g_tls_client_connection_secure_transport_class_init (GTlsClientConnectionSecureTransportClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;
  GTlsConnectionBaseClass *base_connection_class = G_TLS_CONNECTION_BASE_CLASS (klass);
  GTlsConnectionSecureTransportClass *secure_connection_class = (GTlsConnectionSecureTransportClass *) klass;

  gobject_class->set_property = g_tls_client_connection_secure_transport_set_property;
  gobject_class->get_property = g_tls_client_connection_secure_transport_get_property;
  gobject_class->finalize = g_tls_client_connection_secure_transport_finalize;

  g_object_class_override_property (gobject_class, PROP_ACCEPTED_CAS, "accepted-cas");
  g_object_class_override_property (gobject_class, PROP_SERVER_IDENTITY, "server-identity");
  g_object_class_override_property (gobject_class, PROP_USE_SSL3, "use-ssl3");
  g_object_class_override_property (gobject_class, PROP_VALIDATION_FLAGS, "validation-flags");

  base_connection_class->handshake = g_tls_client_connection_secure_transport_handshake;
  secure_connection_class->retrieve_cas = g_tls_client_connection_secure_transport_retrieve_cas;
}

static void
g_tls_client_connection_secure_transport_client_connection_interface_init (GTlsClientConnectionInterface *iface)
{
  iface->copy_session_state = g_tls_client_connection_secure_transport_copy_session_state;
}

static gboolean
g_tls_client_connection_secure_transport_initable_init (GInitable *initable, GCancellable *cancellable, GError **error)
{
  return TRUE;
}

static void
g_tls_client_connection_secure_transport_initable_interface_init (GInitableIface *iface)
{
  iface->init = g_tls_client_connection_secure_transport_initable_init;
}

static void
g_tls_client_connection_secure_transport_init (GTlsClientConnectionSecureTransport *self)
{
  GTlsConnectionSecureTransport *secure = G_TLS_CONNECTION_SECURE_TRANSPORT (self);

  secure->authentication_mode = G_TLS_AUTHENTICATION_REQUIRED;
}
