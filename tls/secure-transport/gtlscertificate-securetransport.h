/*
 * gtlscertificate-securetransport.h
 *
 * Copyright (C) 2018 Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __G_TLS_CERTIFICATE_SECURE_TRANSPORT_H__
#define __G_TLS_CERTIFICATE_SECURE_TRANSPORT_H__

#include <gio/gio.h>

#include <Security/Security.h>
#include <Security/SecKey.h>
#include <Security/SecCertificate.h>

G_BEGIN_DECLS

#define G_TYPE_TLS_CERTIFICATE_SECURE_TRANSPORT (g_tls_certificate_secure_transport_get_type ())
G_DECLARE_FINAL_TYPE (GTlsCertificateSecureTransport, g_tls_certificate_secure_transport,
                      G, TLS_CERTIFICATE_SECURE_TRANSPORT, GTlsCertificate)

GTlsCertificate *           g_tls_certificate_secure_transport_new                  (GBytes * bytes, GTlsCertificate *issuer);
GTlsCertificate *           g_tls_certificate_secure_transport_new_with_cert        (SecCertificateRef cert_ref, GTlsCertificate *issuer);

SecKeyRef                   g_tls_certificate_secure_transport_get_key              (GTlsCertificateSecureTransport *self);
SecCertificateRef           g_tls_certificate_secure_transport_get_cert             (GTlsCertificateSecureTransport *self);
GBytes *                    g_tls_certificate_secure_transport_get_cert_bytes       (GTlsCertificateSecureTransport *self);
GBytes *                    g_tls_certificate_secure_transport_get_issuer_sequence  (GTlsCertificateSecureTransport *self);
GBytes *                    g_tls_certificate_secure_transport_get_subject_sequence (GTlsCertificateSecureTransport *self);
CFMutableArrayRef           g_tls_certificate_secure_transport_to_cert_chain        (GTlsCertificateSecureTransport * self);

GTlsCertificateFlags        g_tls_certificate_secure_transport_verify_internal      (GTlsCertificateSecureTransport *cert, GSocketConnectable *identity, const gchar * purpose, GList * trusted_cas);

G_END_DECLS

#endif /* __G_TLS_CERTIFICATE_SECURE_TRANSPORT_H__ */

