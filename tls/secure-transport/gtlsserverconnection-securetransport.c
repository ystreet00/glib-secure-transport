/*
 * gtlsserverconnection-securetransport.c
 *
 * Copyright (C) 2018 Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "config.h"

#include "gtlsserverconnection-securetransport.h"
#include "gtlscertificate-securetransport.h"
#include "gtlsdatabase-securetransport.h"
#include "gtlsutils-securetransport.h"

enum {
  PROP_AUTHENTICATION_MODE = 1
};

struct _GTlsServerConnectionSecureTransport {
  GTlsConnectionSecureTransport parent;
};

typedef struct _GTlsServerConnectionSecureTransportPrivate {
  GTlsAuthenticationMode authentication_mode;
} GTlsServerConnectionSecureTransportPrivate;

static void g_tls_server_connection_secure_transport_initable_interface_init (GInitableIface *iface);
static void g_tls_server_connection_secure_transport_server_connection_interface_init (GTlsServerConnectionInterface *iface);

G_DEFINE_TYPE_WITH_CODE (GTlsServerConnectionSecureTransport, g_tls_server_connection_secure_transport, G_TYPE_TLS_CONNECTION_SECURE_TRANSPORT,
                         G_ADD_PRIVATE (GTlsServerConnectionSecureTransport)
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE, g_tls_server_connection_secure_transport_initable_interface_init)
                         G_IMPLEMENT_INTERFACE (G_TYPE_TLS_SERVER_CONNECTION, g_tls_server_connection_secure_transport_server_connection_interface_init))

static void
g_tls_server_connection_secure_transport_set_property (GObject * obj, guint property_id, const GValue *value, GParamSpec *pspec)
{
  GTlsConnectionSecureTransport *secure = G_TLS_CONNECTION_SECURE_TRANSPORT (obj);

  switch (property_id) {
    case PROP_AUTHENTICATION_MODE:
      secure->authentication_mode = g_value_get_enum (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, property_id, pspec);
      break;
  }
}

static void
g_tls_server_connection_secure_transport_get_property (GObject * obj, guint property_id, GValue *value, GParamSpec *pspec)
{
  GTlsConnectionSecureTransport *secure = G_TLS_CONNECTION_SECURE_TRANSPORT (obj);

  switch (property_id) {
    case PROP_AUTHENTICATION_MODE:
      g_value_set_enum (value, secure->authentication_mode);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, property_id, pspec);
      break;
  }
}

static SSLAuthenticate
authentication_mode_to_ssl (GTlsAuthenticationMode mode)
{
  switch (mode) {
    case G_TLS_AUTHENTICATION_NONE:
      return kNeverAuthenticate;
    case G_TLS_AUTHENTICATION_REQUESTED:
      return kTryAuthenticate;
    case G_TLS_AUTHENTICATION_REQUIRED:
      return kAlwaysAuthenticate;
    default:
      g_assert_not_reached ();
      return kAlwaysAuthenticate;
  }
}

static GTlsConnectionBaseStatus
g_tls_server_connection_secure_transport_handshake (GTlsConnectionBase *tls, GCancellable *cancellable, GError **error)
{
  GTlsServerConnectionSecureTransport *self = G_TLS_SERVER_CONNECTION_SECURE_TRANSPORT (tls);
  GTlsConnectionSecureTransport *base = G_TLS_CONNECTION_SECURE_TRANSPORT (self);
  OSStatus status;

  if (!base->context) {
    base->context = SSLCreateContext(NULL, kSSLServerSide, kSSLStreamType);
    status = SSLSetClientSideAuthenticate (base->context, authentication_mode_to_ssl (base->authentication_mode));
    if (status == errSecSuccess) {
    /* FIXME: iOS requires adding distinguished names directly with
     * SSLAddDistinguishedName() */
#if TARGET_OS_MAC && !TARGET_OS_IPHONE
      GTlsDatabase *database = g_tls_connection_get_database (G_TLS_CONNECTION (tls));
      GTlsDatabaseSecureTransport *secure_database = G_TLS_DATABASE_SECURE_TRANSPORT (database);
      CFArrayRef anchors = NULL;

      anchors = g_tls_database_secure_transport_to_cert_array (secure_database);

      if (anchors) {
        status = SSLSetCertificateAuthorities (base->context, anchors, TRUE);
        CFRelease (anchors);
      }
#endif
    }

    if (status != errSecSuccess) {
      char *msg = osstatus_to_string (status);
      g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC, "%s", msg);
      g_free (msg);
      return G_TLS_CONNECTION_BASE_ERROR;
    }
  }

  /* FIXME: set session options, tls 1.2, vs 1.3, vs etc */

  return G_TLS_CONNECTION_BASE_CLASS (g_tls_server_connection_secure_transport_parent_class)->handshake (tls, cancellable, error);
}

static void
g_tls_server_connection_secure_transport_class_init (GTlsServerConnectionSecureTransportClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;
  GTlsConnectionBaseClass *base_connection_class = G_TLS_CONNECTION_BASE_CLASS (klass);

  gobject_class->set_property = g_tls_server_connection_secure_transport_set_property;
  gobject_class->get_property = g_tls_server_connection_secure_transport_get_property;

  g_object_class_override_property (gobject_class, PROP_AUTHENTICATION_MODE, "authentication-mode");

  base_connection_class->handshake = g_tls_server_connection_secure_transport_handshake;
}

static gboolean
g_tls_server_connection_secure_transport_initable_init (GInitable *initable, GCancellable *cancellable, GError **error)
{
  return TRUE;
}

static void
g_tls_server_connection_secure_transport_initable_interface_init (GInitableIface *iface)
{
  iface->init = g_tls_server_connection_secure_transport_initable_init;
}

static void
g_tls_server_connection_secure_transport_server_connection_interface_init (GTlsServerConnectionInterface *iface)
{
}

static void
g_tls_server_connection_secure_transport_init (GTlsServerConnectionSecureTransport *self)
{
  GTlsConnectionSecureTransport *secure = G_TLS_CONNECTION_SECURE_TRANSPORT (self);

  secure->authentication_mode = G_TLS_AUTHENTICATION_REQUESTED;
}
